namespace ACE
{
    class BulkInterestGridConfigParser
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;
        private BASE_OPTIONS : any = 
        {
            basePath : "ACE.Extensions.BulkInterestLoader"
        };

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, this.BASE_OPTIONS, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var gridConfigs = this._options.gridConfigsMap;
            
            ACE.Remoting.call("BulkInterestLoaderController.getGridConfigs", [gridConfigs, this._options.basePath], 
            function(result, eventData)
            {
                if (eventData.status)
                {
                    $.each(result, function(field, columns)
                    {
                        this._context.local.state.put(field, columns.sort(function(col1, col2)
                        { 
                            return col1.order - col2.order; 
                        }));
                    }.bind(this));
                }
                
                //Execute even if we get an error so as to let other form components initialize correctly
                callback(true);
            }.bind(this), null, null);
        }
    }

    ACE.AML.ControllerManager.register("extensions:BulkInterestGridConfigParser", BulkInterestGridConfigParser, true);
}