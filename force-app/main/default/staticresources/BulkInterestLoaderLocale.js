//=============================================================================
/**
 * Name: BulkInterestLoaderLocale.js
 * Description: Custom locale class to override the en_US locale bundle strings for client customization. Used to expose the locale keys
 * 				available for the client to override
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Services Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//============================================================================
(function()
{
    //ACE.Locale.loadBundle(ACE.Salesforce.userLocale);

	ACE.Locale.registerBundle
	(
		"en_US",
		{
            STEP1_LABEL                     : "Step 1 of 5: Upload File",
            STEP2_LABEL                     : "Step 2 of 5: Review Recommended Matches",
            STEP3_LABEL                     : "Step 3 of 5: Review Interests",
            STEP4_LABEL                     : "Step 4 of 5: Review Expiring records",
            STEP5_LABEL                     : "Step 5 of 5: Review and Submit",
            STEP5_LABEL_MATCHED             : "Records to be Created/Updated",
            STEP5_LABEL_EXPIRED             : "Records to be Expired",
            STEP5_LABEL_NOT_FOUND           : "Records Not Found",
            STEP1_DESC                      : "Select a CSV file to upload. Please ensure that your file contains 'Contact Name' and 'Ticker Name' columns, optionally you can also include 'Contact Email' and 'Company Name' columns for additional matching.",
            STEP2_DESC                      : "The following records were not directly found but have similarity to ones in the CRM either because the Ticker or Company was similar to a record in the CRM. Select the matches you want to sync. All unselected matches will be ignored and counted as errors.",
            RECOMMENDED_MATCHES_GRID_FILTER : "Enter text to filter",
            STEP3_DESC                      : "The following were matched successfully and will be created in the CRM.",
            STEP4_DESC                      : "Please review the following interests in the CRM for the below Contacts. Using the checkboxes below, select the rows you desire to expire. Unselected rows in this step will remain as Interests for the Contact at the end of your upload operation.",
            STEP5_DESC_MATCHED              : "The following were matched successfully and will be created in the CRM.",
            STEP5_DESC_EXPIRED              : "The following interests will be expired in the CRM after clicking.",
            STEP5_DESC_NOT_FOUND            : "The following were not found in the CRM and will be downloaded as a list upon submitting. Use the Trash Button below to remove the row from your downloaded list.",
            STEP1_ERROR                     : "Please ensure that your file contains Contact and Ticker Name columns.",
            CONTACT_NAME_COLUMN             : "Contact Name",
            TICKER_NAME_COLUMN              : "Ticker Name",
            COMPANY_NAME_COLUMN             : "Company Name",
            CONTACT_EMAIL_COLUMN            : "Contact Email",
            SUBMIT_BUTTON                   : "Submit",
            DOWNLOAD_EXCEPTIONS_BUTTON      : "Download Exceptions",
            ERROR_CONTACT_NAME_MISSING      : "Contact Not Found",
            ERROR_TICKER_NAME_MISSING       : "Interest Subject Not Found",
            EXCEPTIONS_FILE_NAME            : "Exceptions"
        }
	);
    
    ACE.Locale.loadBundle(ACE.Salesforce.userLocale);
})();

//# sourceURL=BulkInterestLoaderLocale.js