namespace ACE
{
    var BASECONFIG = {
        dataSource : "",
        notFoundRecordsData : "",
        worksheetTitle : "Data Export",
        errorColumnName : "Error"
    }
    
    class DownloadExceptionsController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;

        private __fileName : string = ACE.Locale.getBundleValue("EXCEPTIONS_FILE_NAME");
    
        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASECONFIG, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var notFoundRecords = this._context.local.state.get(this._options.notFoundRecordsData);
            var interests : object[] = this._context.local.state.get(this._options.dataSource);
            // Convert remaining records in the Records Not Found grid into a index -> error map
            var interestIndexToErrorReason : {[index : number] : string} = notFoundRecords.reduce(function(accumulator, currentValue)
            {
                var index = currentValue.id;
                var errorReason = currentValue.errorReason;
                accumulator[index] = errorReason;

                return accumulator;
            }, {});

            var exceptionsTable : string[][] = [];

            if (Object.keys(interestIndexToErrorReason).length > 0)
            {
                var columnsRow : string[] = [...Object.keys(interests[0]), this._options.errorColumnName];
                exceptionsTable.push(columnsRow);

                // Build the table which will be converted into an excel spreadsheet
                for (let index in interestIndexToErrorReason)
                {
                    var exceptionRow : string[] = [];
                    for (let key in interests[index])
                    {
                        exceptionRow.push(interests[index][key]);
                    }
                    exceptionRow.push(interestIndexToErrorReason[index]);
                    exceptionsTable.push(exceptionRow);
                }

                // Create and download the excel spreadsheet TODO: Fix any types
                var workbook : any = ACE.Excel.Builder.createWorkbook();
                var worksheetTitle : string = this._options.worksheetTitle;
                var worksheet : any = workbook.createWorksheet({ name: worksheetTitle });
                worksheet.setData(exceptionsTable);
                workbook.addWorksheet(worksheet);

                var fileName : string = this.__fileName;
                /*if (fileName === "")
                {
                    fileName = this._context.local.state.get("interestsFile")[0].name.split(".csv")[0] + " (exceptions)";
                }*/

                ACE.Excel.Builder.createAndSaveFile(workbook, null, fileName + ".xlsx");
            }
            callback(true);
        }
    }

    ACE.AML.ControllerManager.register("extensions:DownloadExceptionsController", DownloadExceptionsController, true);
}