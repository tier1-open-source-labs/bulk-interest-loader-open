var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var ACE;
(function (ACE) {
    var BulkInterestGridConfigParser = (function () {
        function BulkInterestGridConfigParser(context) {
            this.BASE_OPTIONS = {
                basePath: "ACE.Extensions.BulkInterestLoader"
            };
            this._context = context;
            this._options = $.extend({}, this.BASE_OPTIONS, context.options);
        }
        BulkInterestGridConfigParser.prototype.execute = function (e, eventData, callback) {
            var gridConfigs = this._options.gridConfigsMap;
            ACE.Remoting.call("BulkInterestLoaderController.getGridConfigs", [gridConfigs, this._options.basePath], function (result, eventData) {
                if (eventData.status) {
                    $.each(result, function (field, columns) {
                        this._context.local.state.put(field, columns.sort(function (col1, col2) {
                            return col1.order - col2.order;
                        }));
                    }.bind(this));
                }
                callback(true);
            }.bind(this), null, null);
        };
        return BulkInterestGridConfigParser;
    }());
    ACE.AML.ControllerManager.register("extensions:BulkInterestGridConfigParser", BulkInterestGridConfigParser, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASE_CONFIG = {
        columns: [],
        dataProvider: [],
        enableGrouping: false,
        allowRowSelection: false,
        groupByField: "",
        textFilter: "",
        enableCheckBox: false,
        enableQuickAdd: false,
        quickAddConfigPath: "ACE.Core.QuickAdd.Layouts.QAC",
        enableDeleteRow: false,
        itemsCounter: null,
        uncheckableGroups: [],
        headerRowHeight: 40,
        gridStep: 0,
        accountIdParameter: "entityId",
        relatedDataProvider: ""
    };
    var BulkInterestsGridController = (function () {
        function BulkInterestsGridController(context) {
            this._updateRelatedDataProvider = function (selectedRowData) {
                var altItemIdsToRemove = [];
                var oldRelatedItemsUpdated = this._oldRelatedItems;
                for (var i = 0; i < selectedRowData.length; i++) {
                    var item = selectedRowData[i];
                    if (item.altSuggestionId) {
                        altItemIdsToRemove.push(item.altSuggestionId);
                    }
                }
                var altItemsToAdd = [];
                var oldRelatedItemsIds = Object.keys(this._oldRelatedItems);
                for (var i = 0; i < oldRelatedItemsIds.length; i++) {
                    var itemIndex = altItemIdsToRemove.indexOf(oldRelatedItemsIds[i]);
                    if (itemIndex === -1) {
                        altItemsToAdd.push(this._oldRelatedItems[oldRelatedItemsIds[i]]);
                        var property = oldRelatedItemsIds[i];
                        delete oldRelatedItemsUpdated[property];
                    }
                }
                var altDataProvider = this._context.local.state.get(this._options.relatedDataProvider);
                altDataProvider = altDataProvider.filter(function (item) {
                    var index = altItemIdsToRemove.indexOf(item.id);
                    if (index !== -1) {
                        oldRelatedItemsUpdated[item.id] = item;
                    }
                    return index === -1;
                });
                altDataProvider = altDataProvider.concat(altItemsToAdd);
                this._oldRelatedItems = oldRelatedItemsUpdated;
                this._context.local.state.put(this._options.relatedDataProvider, altDataProvider);
            };
            this._updateStateWithQacResult = function (contactName) {
                if (contactName === void 0) { contactName = ""; }
                var items = this._grid.dataProvider.getItems();
                for (var i = 0; i < items.length; i++) {
                    if (items[i].oldValue === contactName) {
                        this._oldDeletedItems.push(items[i]);
                        this._grid.dataProvider.deleteItem(items[i].id);
                        break;
                    }
                }
                this._updateRelatedDataProvider(this._oldDeletedItems);
            };
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
            this._idsToItems = {};
            this._oldRelatedItems = {};
            this._oldDeletedItems = [];
            this._accountId = ACEUtil.getURLParameter(this._options.accountIdParameter);
        }
        BulkInterestsGridController.prototype.init = function () {
            this.createChildren();
            this._context.readyHandler();
        };
        BulkInterestsGridController.prototype.createChildren = function () {
            this._grid = new ACEDataGrid(this._context.target, {
                forceFitColumns: true,
                explicitInitialization: true,
                enableColumnReorder: false,
                allowRowSelection: this._options.allowRowSelection,
                textFilter: $(this._options.textFilter).find('input'),
                editable: false,
                enableGroupBy: this._options.enableGrouping,
                pinGroupByToLeft: true,
                headerRowHeight: this._options.headerRowHeight,
                autoEdit: false,
                itemRendererFilterFunctions: {}
            });
            var gridCols = this.addFormattersAndEditors(this._options.columns);
            this._grid.setColumns(gridCols, {
                "QuickAddRenderer": new ItemRenderer(function (row, cell, value, columnDef, dataContext) {
                    return "<button class='quickAddButton quickAddContactBtn' style='height:20px;'/>";
                }),
                "DeleteRowRenderer": new ItemRenderer(function (row, cell, value, columnDef, dataContext) {
                    return "<button class='deleteRowButton header-btn-icon header-btn-delete'></button>";
                })
            });
            this._grid.addItems(this._options.dataProvider);
            this._grid.grid.init();
            this._grid.grid.invalidate();
            this._grid.grid.resizeCanvas();
            var groupByGetter = function (item) {
                var mergeStr = "{" + this._options.groupByField + "}";
                var mergeVal = ACE.StringUtil.mergeParameters(mergeStr, item);
                return mergeVal;
            }.bind(this);
            var groupByFormatter = function (aggregate) {
                var groupByDiv = "<div class='flex-parent is-horizontal' style='align-items:center;'>";
                var groupByLabel = aggregate.value;
                if (!this._options.uncheckableGroups.includes(groupByLabel) && this._options.enableCheckBox) {
                    groupByDiv += "<input ";
                    var numItemsChecked = aggregate.rows.filter(function (item) {
                        return item._customIsSelected;
                    }).length;
                    if (numItemsChecked === aggregate.rows.length) {
                        groupByDiv += " checked='checked' ";
                    }
                    else if (numItemsChecked > 0) {
                    }
                    groupByDiv += "data='groupByCheckBox' class='groupByCheckBox'" +
                        "type='checkBox' style='margin-right: 5px;'></input>" + groupByLabel;
                }
                else {
                    groupByDiv += groupByLabel;
                }
                groupByDiv += "</div>";
                return groupByDiv;
            }.bind(this);
            this._grid.dataProvider.setGrouping([
                {
                    getter: groupByGetter,
                    formatter: groupByFormatter,
                    aggregators: [],
                    aggregateCollapsed: false,
                    lazyTotalsCalculation: true
                }
            ]);
            this._grid.onRowSelectionChange(function (e, args) {
                if (this._options.relatedDataProvider !== "") {
                    this._updateRelatedDataProvider(e.data);
                }
                this._context.local.state.put("latestChangesStep", this._options.gridStep);
            }.bind(this));
            this._grid.grid.onClick.subscribe(function (e, args) {
                var clickedItem = args.grid.getDataItem(args.row);
                if ($(e.target).attr("data") == "groupByCheckBox" && clickedItem.groupingKey) {
                    this._grid.dataProvider.beginUpdate();
                    $.each(clickedItem.rows, function (index, item) {
                        item._customIsSelected = e.target.checked;
                        this._grid.dataProvider.updateItem(item.id, item);
                    }.bind(this));
                    this._grid.dataProvider.endUpdate();
                    this._grid._updateSelectAllCheckBox();
                    if (this._options.relatedDataProvider !== "") {
                        this._updateRelatedDataProvider(this._grid.getSelectedItems());
                    }
                    this._context.local.state.put("latestChangesStep", this._options.gridStep);
                }
                else if ($(e.target).hasClass('quickAddButton')) {
                    var contactName = clickedItem.oldValue;
                    var nameSplit = contactName.split(" ");
                    var firstName = nameSplit.length >= 1 ? nameSplit[0] : "";
                    var lastName = nameSplit.length >= 2 ? nameSplit.splice(1).join() : "";
                    var saveCallback = function (eventData) {
                        this._updateStateWithQacResult(contactName);
                    }.bind(this);
                    ACE.CustomEventDispatcher.trigger("aml-showdialog", {
                        "configPath": this._options.quickAddConfigPath,
                        "isCanvas": true,
                        "state": {
                            "Contact": {
                                "AccountId": this._accountId,
                                "FirstName": firstName,
                                "LastName": lastName
                            },
                            "saveCallback": saveCallback
                        }
                    });
                }
                else if ($(e.target).hasClass('deleteRowButton')) {
                    this._grid.dataProvider.deleteItem(clickedItem.id);
                    if (this._options.itemsCounter) {
                        document.getElementById(this._options.itemsCounter).innerText = this._grid.dataProvider.getItems().length;
                    }
                }
                else if ($(e.target).hasClass('groupByDelete')) {
                    this._grid.dataProvider.beginUpdate();
                    $.each(clickedItem.rows, function (index, item) {
                        this._grid.dataProvider.deleteItem(item.id);
                    }.bind(this));
                    this._grid.dataProvider.endUpdate();
                    if (this._options.itemsCounter) {
                        document.getElementById(this._options.itemsCounter).innerText = this._grid.dataProvider.getItems().length;
                    }
                }
            }.bind(this));
        };
        BulkInterestsGridController.prototype.resizeHandler = function () {
            if (this._grid) {
                this._grid.grid.resizeCanvas();
            }
        };
        BulkInterestsGridController.prototype.showHandler = function () {
            if (this._grid) {
                this._grid.setOption('textFilter', $(this._options.textFilter).find('input'));
                this._grid.grid.resizeCanvas();
            }
        };
        BulkInterestsGridController.prototype.destroy = function () { };
        BulkInterestsGridController.prototype.setFocus = function () { };
        BulkInterestsGridController.prototype.clearState = function () { };
        BulkInterestsGridController.prototype.resetState = function () { };
        BulkInterestsGridController.prototype.setOption = function (optionName, value) {
            switch (optionName) {
                case "dataProvider":
                    this._idsToItems = value.reduce(function (accumulator, currentValue) {
                        var id = currentValue.id;
                        accumulator[id] = currentValue;
                        return accumulator;
                    }, {});
                    this._grid.setItems(value);
                    if (this._options.itemsCounter) {
                        document.getElementById(this._options.itemsCounter).innerText = this._grid.dataProvider.getItems().length;
                    }
                    this._grid.grid.invalidate();
                    break;
                case "stepNumber":
                    if (value === 1) {
                        this._oldDeletedItems = [];
                        this._oldRelatedItems = {};
                    }
                    break;
            }
        };
        BulkInterestsGridController.prototype.addFormattersAndEditors = function (colsArray) {
            $.each(colsArray, function (index, column) {
                if (!column.fieldType) {
                    return true;
                }
                switch (column.fieldType.toLowerCase()) {
                    case "lookup":
                        column.editor = Slick.Editors.AutoComplete;
                        break;
                    case "date":
                        column.editor = Slick.Editors.Date;
                        break;
                    case "picklist":
                        column.editorDataProvider = column.additionalConfig.dataProvider;
                        column.editor = Slick.Editors.ACEMenuButton;
                        break;
                    case "quickadd":
                        column.formatter = "QuickAddRenderer";
                        break;
                    default:
                        column.editor = Slick.Editors.Text;
                }
            });
            if (this._options.enableCheckBox) {
                var checkBoxCol = {
                    sortable: false,
                    fieldType: "CHECKBOX",
                    id: "checkcolumn",
                    field: "_customIsSelected",
                    maxWidth: 30,
                    formatter: "CheckBoxSelectorItemRenderer",
                    editor: Slick.Editors.Checkbox,
                    name: "",
                    order: this._options.undoColumnPosition,
                    defaultValue: null,
                    defaultId: null,
                    visible: true,
                    sObjectName: null,
                    picklistOptions: null,
                    disabled: false,
                    placeHolder: null,
                    innerCSS: null,
                    hoverText: null,
                    minWidth: 30,
                    additionalConfig: null,
                    editorDataProvider: null
                };
                colsArray.unshift(checkBoxCol);
            }
            if (this._options.enableQuickAdd) {
                var quickAddColumn = {
                    id: "quickAddColumn",
                    field: "_customWasAdded",
                    sortable: true,
                    maxWidth: 30,
                    formatter: "QuickAddRenderer",
                    fieldType: "QUICKADD",
                    name: "",
                    order: this._options.undoColumnPosition,
                    defaultValue: null,
                    defaultId: null,
                    visible: true,
                    sObjectName: null,
                    picklistOptions: null,
                    disabled: false,
                    placeHolder: null,
                    innerCSS: null,
                    hoverText: null,
                    editor: null,
                    minWidth: 30,
                    additionalConfig: null,
                    editorDataProvider: null
                };
                colsArray.push(quickAddColumn);
            }
            if (this._options.enableDeleteRow) {
                var deleteRowColumn = {
                    id: "deleteRowColumn",
                    field: "_deleted",
                    sortable: true,
                    maxWidth: 30,
                    formatter: "DeleteRowRenderer",
                    fieldType: "",
                    name: "",
                    order: this._options.undoColumnPosition,
                    defaultValue: null,
                    defaultId: null,
                    visible: true,
                    sObjectName: null,
                    picklistOptions: null,
                    disabled: false,
                    placeHolder: null,
                    innerCSS: null,
                    hoverText: null,
                    editor: null,
                    minWidth: 30,
                    additionalConfig: null,
                    editorDataProvider: null
                };
                colsArray.push(deleteRowColumn);
            }
            return colsArray;
        };
        ;
        return BulkInterestsGridController;
    }());
    ACE.AML.ControllerManager.register("extensions:BulkInterestsGridController", BulkInterestsGridController, false);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASECONFIG = {
        dataSource: "",
        notFoundRecordsData: "",
        worksheetTitle: "Data Export",
        errorColumnName: "Error"
    };
    var DownloadExceptionsController = (function () {
        function DownloadExceptionsController(context) {
            this.__fileName = ACE.Locale.getBundleValue("EXCEPTIONS_FILE_NAME");
            this._context = context;
            this._options = $.extend({}, BASECONFIG, context.options);
        }
        DownloadExceptionsController.prototype.execute = function (e, eventData, callback) {
            var notFoundRecords = this._context.local.state.get(this._options.notFoundRecordsData);
            var interests = this._context.local.state.get(this._options.dataSource);
            var interestIndexToErrorReason = notFoundRecords.reduce(function (accumulator, currentValue) {
                var index = currentValue.id;
                var errorReason = currentValue.errorReason;
                accumulator[index] = errorReason;
                return accumulator;
            }, {});
            var exceptionsTable = [];
            if (Object.keys(interestIndexToErrorReason).length > 0) {
                var columnsRow = __spreadArray(__spreadArray([], Object.keys(interests[0])), [this._options.errorColumnName]);
                exceptionsTable.push(columnsRow);
                for (var index in interestIndexToErrorReason) {
                    var exceptionRow = [];
                    for (var key in interests[index]) {
                        exceptionRow.push(interests[index][key]);
                    }
                    exceptionRow.push(interestIndexToErrorReason[index]);
                    exceptionsTable.push(exceptionRow);
                }
                var workbook = ACE.Excel.Builder.createWorkbook();
                var worksheetTitle = this._options.worksheetTitle;
                var worksheet = workbook.createWorksheet({ name: worksheetTitle });
                worksheet.setData(exceptionsTable);
                workbook.addWorksheet(worksheet);
                var fileName = this.__fileName;
                ACE.Excel.Builder.createAndSaveFile(workbook, null, fileName + ".xlsx");
            }
            callback(true);
        };
        return DownloadExceptionsController;
    }());
    ACE.AML.ControllerManager.register("extensions:DownloadExceptionsController", DownloadExceptionsController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var ExpiredInterestsController = (function () {
        function ExpiredInterestsController(context) {
            this.BASE_OPTIONS = {
                property: "expiringInterests",
                contIdField: "T1C_Base__Contact__c",
                intSubIdField: "T1C_Base__Interest_Subject__c",
                fieldPrefix: "interest",
                dataProviderProperty: "foundRecords.gridData",
                accountIdParameter: "entityId",
                latestStepWithChanges: "latestChangesStep",
                gridStep: 0
            };
            this._context = context;
            this._options = $.extend({}, this.BASE_OPTIONS, context.options);
        }
        ExpiredInterestsController.prototype.execute = function (e, eventData, callback) {
            var interestsData = this._context.local.state.get(this._options.dataProviderProperty);
            var interests = [];
            var contactIds = [];
            this._context.local.state.put(this._options.latestStepWithChanges, this._options.gridStep);
            if (this._options.fieldPrefix && this._options.fieldPrefix.length > 0) {
                for (var i = 0; i < interestsData.length; i++) {
                    var interest = interestsData[i];
                    contactIds.push(interest[this._options.fieldPrefix]["T1C_Base__Contact__c"]);
                    if (interest._customIsSelected) {
                        interests.push(interest[this._options.fieldPrefix]);
                    }
                }
            }
            var accountId = ACEUtil.getURLParameter(this._options.accountIdParameter);
            ACE.Remoting.call("BulkInterestLoaderController.getExpiringInterests", [interests, contactIds, accountId, this._options.contIdField, this._options.intSubIdField], function (result, eventData) {
                if (eventData.status) {
                    this._context.local.state.put(this._options.property, result);
                    callback(true);
                }
                else {
                    console.log("getExpiringInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        };
        return ExpiredInterestsController;
    }());
    ACE.AML.ControllerManager.register("extensions:ExpiredInterestsController", ExpiredInterestsController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASE_OPTIONS = {
        dataProviderProperty: "filteredExpiredRecords"
    };
    var ExpiredInterestsSubmitController = (function () {
        function ExpiredInterestsSubmitController(context) {
            this._context = context;
            this._options = $.extend({}, BASE_OPTIONS, context.options);
        }
        ExpiredInterestsSubmitController.prototype.execute = function (e, eventData, callback) {
            var expiredInterests = this._context.local.state.get(this._options.dataProviderProperty).map(function (interest) {
                return interest.data;
            });
            ACE.Remoting.call("BulkInterestLoaderController.expireInterests", [expiredInterests], function (result, eventData) {
                if (eventData.status) {
                    window.close();
                    callback(true);
                }
                else {
                    console.log("expireInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        };
        return ExpiredInterestsSubmitController;
    }());
    ACE.AML.ControllerManager.register("extensions:ExpiredInterestsSubmitController", ExpiredInterestsSubmitController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASECONFIG = {
        gridStep: 0
    };
    var FileUploadController = (function () {
        function FileUploadController(context) {
            this._columnNames = {
                "mandatoryCols": [ACE.Locale.getBundleValue("CONTACT_NAME_COLUMN"), ACE.Locale.getBundleValue("TICKER_NAME_COLUMN")],
                "optionalCols": [ACE.Locale.getBundleValue("COMPANY_NAME_COLUMN"), ACE.Locale.getBundleValue("CONTACT_EMAIL_COLUMN")]
            };
            this._context = context;
            this._options = $.extend({}, BASECONFIG, this._columnNames, context.options);
        }
        FileUploadController.prototype.execute = function (e, eventData, callback) {
            var fileReader = new FileReader();
            var errored = false;
            this._context.local.state.put("latestChangesStep", this._options.gridStep);
            fileReader.onload = function (fileEvent) {
                var file = fileEvent.target.result;
                var lines = file.split(/\r?\n/);
                var columns = lines[0].split(",");
                var colToIndex = {};
                for (var i = 0; i < this._options.mandatoryCols.length; i++) {
                    var colIndex = columns.indexOf(this._options.mandatoryCols[i]);
                    colToIndex[this._options.mandatoryCols[i]] = colIndex;
                    if (colIndex === -1) {
                        errored = true;
                    }
                }
                for (var i = 0; i < this._options.optionalCols.length; i++) {
                    colToIndex[this._options.optionalCols[i]] = columns.indexOf(this._options.optionalCols[i]);
                }
                this._context.local.state.put("errored", errored);
                if (errored) {
                    callback(false);
                }
                else {
                    var interests = lines.slice(1, lines.length - 1).map(function (line) {
                        var interest = {};
                        var lineColumns = line.split(/,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);
                        for (var i = 0; i < this._options.mandatoryCols.length; i++) {
                            interest[this._options.mandatoryCols[i]] = lineColumns[colToIndex[this._options.mandatoryCols[i]]].trim().replace(/(^\"|\"$)/g, "");
                        }
                        for (var i = 0; i < this._options.optionalCols.length; i++) {
                            interest[this._options.optionalCols[i]] = colToIndex[this._options.optionalCols[i]] !== -1 ? lineColumns[colToIndex[this._options.optionalCols[i]]].trim().replace(/(^\"|\"$)/g, "") : null;
                        }
                        return interest;
                    }.bind(this));
                    this._context.local.state.put("interests", interests);
                    callback(true);
                }
            }.bind(this);
            fileReader.readAsText(this._context.local.state.data.interestsFile[0].data);
        };
        return FileUploadController;
    }());
    ACE.AML.ControllerManager.register("extensions:FileUploadController", FileUploadController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASE_CONFIG = {
        matchedRecordsData: "foundRecords.gridData",
        expiredRecordsData: "expiringInterests",
        filteredMatchedRecordsData: "filteredMatchedRecords",
        filteredExpiredRecordsData: "filteredExpiredRecords",
        filteredNotFoundRecordsData: "filteredNotFoundRecords"
    };
    var FormatReviewDataController = (function () {
        function FormatReviewDataController(context) {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }
        FormatReviewDataController.prototype.execute = function (e, eventData, callback) {
            var filteredMatchedRecords = this._context.local.state.get(this._options.matchedRecordsData)
                .filter(function (record) {
                return record._customIsSelected;
            });
            var filteredExpiredRecords = this._context.local.state.get(this._options.expiredRecordsData)
                .filter(function (record) {
                return record._customIsSelected;
            });
            var errorReasons = this._context.local.state.get("interestIndexToErrorReason");
            var interests = this._context.local.state.get("interests");
            var erroredRecords = [];
            for (var errorInterestIndex in errorReasons) {
                erroredRecords.push({
                    id: errorInterestIndex,
                    contactName: interests[errorInterestIndex]["Contact Name"],
                    interestSubjectName: interests[errorInterestIndex]["Ticker Name"],
                    errorReason: errorReasons[errorInterestIndex]
                });
            }
            this._context.local.state.put(this._options.filteredMatchedRecordsData, filteredMatchedRecords);
            this._context.local.state.put(this._options.filteredExpiredRecordsData, filteredExpiredRecords);
            this._context.local.state.put(this._options.filteredNotFoundRecordsData, erroredRecords);
            callback(true);
        };
        return FormatReviewDataController;
    }());
    ACE.AML.ControllerManager.register("extensions:FormatReviewDataController", FormatReviewDataController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var LoadWindowController = (function () {
        function LoadWindowController(context) {
            this._context = context;
            this._options = context.options;
        }
        LoadWindowController.prototype.execute = function (e, eventData, callback) {
            this._context.local.eventDispatcher.on("ready", function (e, eventData) {
                this._context.local.state.put("windowIsReady", true);
            }.bind(this));
            ACE.CustomEventDispatcher.on("childApplicationBridgeLoaded", function () {
                return false;
            });
            callback(true);
        };
        return LoadWindowController;
    }());
    ACE.AML.ControllerManager.register("extensions:LoadWindowController", LoadWindowController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASE_CONFIG = {
        dataProviderProperty: "filteredMatchedRecords"
    };
    var MatchedInterestsSubmitController = (function () {
        function MatchedInterestsSubmitController(context) {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }
        MatchedInterestsSubmitController.prototype.execute = function (e, eventData, callback) {
            var matchedInterests = this._context.local.state.get(this._options.dataProviderProperty).map(function (record) {
                return record.interest;
            });
            ACE.Remoting.call("BulkInterestLoaderController.saveInterests", [matchedInterests], function (result, eventData) {
                if (eventData.status) {
                    callback(true);
                }
                else {
                    console.log("saveInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        };
        return MatchedInterestsSubmitController;
    }());
    ACE.AML.ControllerManager.register("extensions:MatchedInterestsSubmitController", MatchedInterestsSubmitController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASE_CONFIG = {
        accountIdParameter: "entityId",
        latestStepWithChanges: "latestChangesStep",
        outputDataProperty: "foundRecords.gridData",
        gridStep: 0
    };
    var ReviewInterestsController = (function () {
        function ReviewInterestsController(context) {
            this.CONTACT_NAME_COLUMN = ACE.Locale.getBundleValue("CONTACT_NAME_COLUMN");
            this.TICKER_NAME_COLUMN = ACE.Locale.getBundleValue("TICKER_NAME_COLUMN");
            this.ERRORS = {
                "ERROR_CONTACT_NAME_MISSING": ACE.Locale.getBundleValue("ERROR_CONTACT_NAME_MISSING"),
                "ERROR_TICKER_NAME_MISSING": ACE.Locale.getBundleValue("ERROR_TICKER_NAME_MISSING")
            };
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }
        ReviewInterestsController.prototype.execute = function (e, eventData, callback) {
            var interests = this._context.local.state.get("interests");
            var tweakedInterests = JSON.parse(JSON.stringify(interests));
            var fixes = this._context.local.state.get("recommendedMatches").gridData;
            this._context.local.state.put(this._options.latestStepWithChanges, this._options.gridStep);
            for (var i = 0; i < fixes.length; i++) {
                if (fixes[i]._customIsSelected) {
                    var fix = fixes[i];
                    for (var j = 0; j < fix.interestIndices.length; j++) {
                        tweakedInterests[fix.interestIndices[j]][fix.field] = fix.newValue;
                    }
                }
            }
            var accountId = ACEUtil.getURLParameter(this._options.accountIdParameter);
            ACE.Remoting.call("BulkInterestLoaderController.getMatchedRecords", [tweakedInterests, accountId,
                this.CONTACT_NAME_COLUMN, this.TICKER_NAME_COLUMN, this.ERRORS], function (result, eventData) {
                if (eventData.status) {
                    var foundRecordsGridData = result["interests"].map(function (records, index) {
                        var interest = records["interest"];
                        var contactName = records["contactName"];
                        var interestSubjectName = records["interestSubjectName"];
                        var interestSubjectDescription = records["interestSubjectDescription"];
                        return {
                            id: index,
                            interest: interest,
                            contactName: contactName,
                            interestSubjectName: interestSubjectName,
                            interestSubjectDescription: interestSubjectDescription ? interestSubjectDescription : ""
                        };
                    });
                    this._context.local.state.put(this._options.outputDataProperty, foundRecordsGridData);
                    this._context.local.state.put("interestIndexToErrorReason", result["interestIndexToErrorReason"]);
                    callback(true);
                }
                else {
                    console.log("matchInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        };
        return ReviewInterestsController;
    }());
    ACE.AML.ControllerManager.register("extensions:ReviewInterestsController", ReviewInterestsController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var BASE_CONFIG = {
        maxRemotingCallIterations: 10,
        recommendedMatchesProperty: "recommendedMatches.gridData",
        contactsNotFoundProperty: "contactsNotFound.gridData",
        accountIdParameter: "entityId",
        batchSize: 100,
        gridStep: 0
    };
    var ReviewMatchesController = (function () {
        function ReviewMatchesController(context) {
            this.CONTACT_NAME_COLUMN = ACE.Locale.getBundleValue("CONTACT_NAME_COLUMN");
            this.TICKER_NAME_COLUMN = ACE.Locale.getBundleValue("TICKER_NAME_COLUMN");
            this.COMPANY_NAME_COLUMN = ACE.Locale.getBundleValue("COMPANY_NAME_COLUMN");
            this.CONTACT_EMAIL_COLUMN = ACE.Locale.getBundleValue("CONTACT_EMAIL_COLUMN");
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }
        ReviewMatchesController.prototype.execute = function (e, eventData, callback) {
            var interests = this._context.local.state.get("interests");
            var accountId = ACEUtil.getURLParameter(this._options.accountIdParameter);
            this._context.local.state.put("latestChangesStep", this._options.gridStep);
            var numCalls = 0;
            var reviewMatches = function (suggestions, lastQueriedId, numCalls) {
                ACE.Remoting.batch("BulkInterestLoaderController.matchInterests", interests, function (args) {
                    return [args, accountId, this.CONTACT_NAME_COLUMN, this.TICKER_NAME_COLUMN,
                        this.COMPANY_NAME_COLUMN, this.CONTACT_EMAIL_COLUMN, suggestions, lastQueriedId];
                }.bind(this), function (result, eventData) {
                    var suggestionsData = result.map(function (batch) {
                        return batch[0];
                    }).reduce(function (accumulator, currentValue) {
                        return __assign(__assign({}, accumulator), currentValue);
                    }, {});
                    if (eventData) {
                        if (numCalls > this._options.maxRemotingCallIterations) {
                            callback(false);
                        }
                        else if (result[0][2] < 10000) {
                            var recommendedMatchesData = [];
                            var contactsNotFoundData = [];
                            var oldValues = Object.keys(suggestionsData);
                            for (var i = 0; i < oldValues.length; i++) {
                                var error = suggestionsData[oldValues[i]].type.toLowerCase() + suggestionsData[oldValues[i]].field.replace(/\s/g, "");
                                switch (error) {
                                    case "renameTickerName":
                                        error = "Interest Subjects";
                                        break;
                                    case "renameContactName":
                                        error = "Contacts";
                                        break;
                                    case "createContactName":
                                        error = "Contacts Not Found";
                                        break;
                                }
                                var displayValue = suggestionsData[oldValues[i]].newValue +
                                    (suggestionsData[oldValues[i]].extraValue ? " (" + suggestionsData[oldValues[i]].extraValue + ")" : '');
                                if (error === "Contacts Not Found") {
                                    contactsNotFoundData.push({
                                        id: i + "createContactName",
                                        oldValue: oldValues[i],
                                        newValue: oldValues[i],
                                        displayValue: displayValue,
                                        interestIndices: [],
                                        field: suggestionsData[oldValues[i]].field,
                                        error: error,
                                        altSuggestionId: "-1",
                                        _customIsSelected: false
                                    });
                                }
                                else if (error == "Contacts") {
                                    recommendedMatchesData.push({
                                        id: i + "renameContactName",
                                        oldValue: oldValues[i],
                                        newValue: suggestionsData[oldValues[i]].newValue,
                                        displayValue: displayValue,
                                        interestIndices: suggestionsData[oldValues[i]].interestIndices,
                                        field: suggestionsData[oldValues[i]].field,
                                        error: error,
                                        altSuggestionId: i + "createContactName",
                                        _customIsSelected: false
                                    });
                                    contactsNotFoundData.push({
                                        id: i + "createContactName",
                                        oldValue: oldValues[i],
                                        newValue: oldValues[i],
                                        displayValue: displayValue,
                                        interestIndices: [],
                                        field: suggestionsData[oldValues[i]].field,
                                        error: "Contacts Not Found",
                                        altSuggestionId: i + "renameContactName",
                                        _customIsSelected: false
                                    });
                                }
                                else {
                                    recommendedMatchesData.push({
                                        id: i + "renameTickerName",
                                        oldValue: oldValues[i],
                                        newValue: suggestionsData[oldValues[i]].newValue,
                                        displayValue: displayValue,
                                        interestIndices: suggestionsData[oldValues[i]].interestIndices,
                                        field: suggestionsData[oldValues[i]].field,
                                        error: error,
                                        altSuggestionId: "-1",
                                        _customIsSelected: false
                                    });
                                }
                            }
                            this._context.local.state.put(this._options.recommendedMatchesProperty, recommendedMatchesData);
                            this._context.local.state.put(this._options.contactsNotFoundProperty, contactsNotFoundData);
                            callback(true);
                        }
                        else {
                            reviewMatches(suggestionsData, result[0][1], numCalls + 1);
                        }
                    }
                    else {
                        console.log("matchInterests batch remote call failed");
                        callback(false);
                    }
                }.bind(this), {
                    retryOnTimeout: false,
                    batchSize: this._options.batchSize,
                    showProgressBar: true,
                    alwaysShowProgressBar: true,
                    parallel: true
                }, {
                    retryOnTimeout: false,
                    batchSize: this._options.batchSize,
                    showProgressBar: true,
                    alwaysShowProgressBar: true,
                    parallel: true
                });
            }.bind(this);
            reviewMatches({}, "000000000000000000", numCalls);
        };
        return ReviewMatchesController;
    }());
    ACE.AML.ControllerManager.register("extensions:ReviewMatchesController", ReviewMatchesController, true);
})(ACE || (ACE = {}));
var ACE;
(function (ACE) {
    var StepsController = (function () {
        function StepsController(context) {
            this._context = context;
            this._options = context.options;
            this.__stepNumber = 1;
        }
        StepsController.prototype.execute = function (e, eventData, callback) {
            this.__stepNumber = this._context.local.state.get("stepNumber");
            switch (e.type) {
                case this._options.prevStepEvent:
                    if (this.__stepNumber > 1) {
                        this.__stepNumber -= 1;
                        this._context.local.state.put("stepNumber", this.__stepNumber);
                        this._context.local.eventDispatcher.trigger("startStep" + this.__stepNumber);
                    }
                    break;
                case this._options.nextStepEvent:
                    if (this.__stepNumber < 5) {
                        this.__stepNumber += 1;
                        this._context.local.state.put("stepNumber", this.__stepNumber);
                        this._context.local.eventDispatcher.trigger("startStep" + this.__stepNumber);
                    }
                    break;
            }
            callback(true);
        };
        return StepsController;
    }());
    ACE.AML.ControllerManager.register("extensions:StepsController", StepsController, true);
})(ACE || (ACE = {}));
//# sourceMappingURL=BulkInterestLoader.js.map