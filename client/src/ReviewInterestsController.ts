namespace ACE
{
    var BASE_CONFIG = 
    {
        accountIdParameter      : "entityId",
        latestStepWithChanges   : "latestChangesStep",
        outputDataProperty      : "foundRecords.gridData",
        gridStep                : 0
    }

    class ReviewInterestsController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;

        private CONTACT_NAME_COLUMN : string = ACE.Locale.getBundleValue("CONTACT_NAME_COLUMN");
        private TICKER_NAME_COLUMN : string = ACE.Locale.getBundleValue("TICKER_NAME_COLUMN");
        private ERRORS : { [key : string] : string } = {
            "ERROR_CONTACT_NAME_MISSING" : ACE.Locale.getBundleValue("ERROR_CONTACT_NAME_MISSING"),
            "ERROR_TICKER_NAME_MISSING" : ACE.Locale.getBundleValue("ERROR_TICKER_NAME_MISSING")
        }

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            // Tweak interests list to include the suggested fixes that were selected
            var interests : object[] = this._context.local.state.get("interests");
            var tweakedInterests : object[] = JSON.parse(JSON.stringify(interests));
            var fixes : Suggestion[] = this._context.local.state.get("recommendedMatches").gridData;
            this._context.local.state.put(this._options.latestStepWithChanges, this._options.gridStep);

            for (let i = 0; i < fixes.length; i++)
            {
                // Suggested fix was selected by the user
                if (fixes[i]._customIsSelected)
                {
                    var fix : Suggestion = fixes[i];
                    for (let j = 0; j < fix.interestIndices.length; j++)
                    {
                        tweakedInterests[fix.interestIndices[j]][fix.field] = fix.newValue;
                    }
                }
            }

            var accountId : string = ACEUtil.getURLParameter(this._options.accountIdParameter);
 
            // context.services.execute(func name, args).then()
            ACE.Remoting.call("BulkInterestLoaderController.getMatchedRecords", [tweakedInterests, accountId,  
                this.CONTACT_NAME_COLUMN, this.TICKER_NAME_COLUMN, this.ERRORS], function(result, eventData)
            {
                if (eventData.status)
                {
                    var foundRecordsGridData : { [key : string] : any }[] = result["interests"].map(function(records, index)
                    {
                        var interest : object = records["interest"];
                        var contactName : string = records["contactName"];
                        var interestSubjectName : string = records["interestSubjectName"];
                        var interestSubjectDescription : string = records["interestSubjectDescription"];

                        return {
                            id: index,
                            interest: interest,
                            contactName: contactName,
                            interestSubjectName: interestSubjectName,
                            interestSubjectDescription: interestSubjectDescription ? interestSubjectDescription : ""
                        };
                    });

                    this._context.local.state.put(this._options.outputDataProperty, foundRecordsGridData);
                    this._context.local.state.put("interestIndexToErrorReason", result["interestIndexToErrorReason"]);

                    callback(true);
                }
                else
                {
                    console.log("matchInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        }
    }

    ACE.AML.ControllerManager.register("extensions:ReviewInterestsController", ReviewInterestsController, true);
}