declare namespace ACE
{
    export namespace Remoting
    {
        export function call(methodToCall : string, args : any[], 
            callback : Function,
            options : any, 
            additionalOptions : any): void;
        export function batch(methodToCall : string, items : any[], argsFunction : Function,
            callback : Function,
            options : any, 
            additionalOptions : any): void;
    }

    export namespace StringUtil
    {
        export function mergeParameters(mergeStr : String, item : any)
    }


    export namespace Locale
    {
        export function getBundleValue(key : string) : string;
    }

    export namespace LoadingIndicator
    {
        export function hide() : void;
        export function show(message : string) : void;
    }

    export namespace CustomEventDispatcher
    {
        export function on(event:string, handler:Function): void;
        export function trigger(event:string): void;
        export function trigger(event:string, eventData:any)
    }

    export namespace FaultHandlerDialog
    {
        export function show(options:any): void;
    }

    export namespace Excel.Builder
    {
        export function createWorkbook();
        export function createAndSaveFile(workbook: any, options:any, filename:string): void;
    }

    export interface IRemotingEvent
    {
        status:boolean;
    }

    export class Salesforce
    {
        public static sessionId:string;
    }

    export class ACEEvent
    {
        public type:string;
    }

    export const enum Event
    {
        CHILD_APPLICATION_BRIDGE_LOADED = 'childApplicationBridgeLoaded'
    }
}

declare namespace ACE.AML
{
    export class ControllerContext
    {
        readyHandler():void;
        local  : any;
        global : any;
        options : any;
        target : any;
    }

    export namespace ControllerManager
    {
        export function register(key:string, controller:any, force:boolean): void;
    }

    export namespace DOMUtil
    {
        export function load(path:string, target:string, state:object, replace:boolean, options:object): void;
    }

    export const enum Event
    {
        READY = 'ready',
        PROPERTY_CHANGE = 'aml-propertyChange',
        STATE_VALIDATION_CHANGE = 'aml-stateValidationChange',
        UPDATE_GLOBAL_STATE = 'updateGlobalState',
        SHOW_DIALOG = 'aml-showdialog'
    }
}

declare namespace ACEConfirm
{
    export function show(msg:string, title:string, buttons:string[], callback:Function, options:any): void;
}

declare class ACEDataGrid
{
    constructor(target : any, options : any);
    public grid : any;
    public dataProvider : any;
    public setColumns(columns : any) : void;
    public setColumns(columns : any, renderers : any) : void;
    public addItems(dataProvider : any[]) : void;
    public setItems(dataProvider : any[]) : void;
    public setOption(optionName : string, optionValue : any) : void;
    public on(eventName : string, handler : Function) : void;
    public onRowSelectionChange(handler : Function) : void;
    public getSelectedItems() : any[];
    public _updateSelectAllCheckBox() : void;
}

declare namespace ACEUtil
{
    export function getURLParameter(param:string);
}

declare interface IGridFieldConfig
{
   id : string;
   fieldType  : string;
   name  : string;
   order  : number;
   defaultValue : string;
   defaultId : string;
   visible : boolean;
   sObjectName : string;
   field : string;
   sortable : boolean;
   picklistOptions : string[];
   additionalConfig : any;
   disabled : boolean;
   placeHolder : string;
   innerCSS : string;//Used for formatting icon columns
   hoverText : string;
   formatter : any;
   editor : Function;
   editorDataProvider : any[];
   maxWidth : number;
   minWidth : number;
}

declare class ItemRenderer
{
    constructor(callback : Function);
}

declare namespace sforce
{
    export namespace apex
    {
        export function execute(className:string, method:string, params:object, options:object): void;
    }

    export class connection
    {
        public static sessionId:string;
    }

}

declare namespace Slick
{
    export namespace Editors
    {
        export class AutoComplete{}
        export class Date{}
        export class ACEMenuButton{}
        export class Text{}
        export class Checkbox{}
    }

}