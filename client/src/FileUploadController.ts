namespace ACE
{
    var BASECONFIG = {
        gridStep : 0       
    }
    
    class FileUploadController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;
        protected _columnNames : { [key : string] : string[]} = 
        {
            "mandatoryCols" : [ACE.Locale.getBundleValue("CONTACT_NAME_COLUMN"), ACE.Locale.getBundleValue("TICKER_NAME_COLUMN")],
            "optionalCols": [ACE.Locale.getBundleValue("COMPANY_NAME_COLUMN"), ACE.Locale.getBundleValue("CONTACT_EMAIL_COLUMN")]
        };
    
        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASECONFIG, this._columnNames, context.options);

            /*this._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, 
                function(e : ACE.ACEEvent, eventData : any) 
            { 
                if (eventData != null) 
                { 
                    if (eventData.property === "interestsFile" && eventData.newValue.length === 1)
                    {
                        var fileReader: FileReader = new FileReader();
                        var errored: boolean = false;
                        fileReader.onload = function(fileEvent): void
                        {
                            var file: string = fileEvent.target.result as string;
                            var lines: string[] = file.split(/\r?\n/);
                            var columns: string[] = lines[0].split(",");

                            //Map containing Column Index by Column Name
                            //Key -> Column Name
                            //Values -> Field Index
                            var colToIndex: { [key : string] : number } = {};
                            for (let i = 0; i < this._options.mandatoryCols.length; i++)
                            {
                                var colIndex: number = columns.indexOf(this._options.mandatoryCols[i]);
                                colToIndex[this._options.mandatoryCols[i]] = colIndex;
                                if (colIndex === -1)
                                {
                                    errored = true;
                                }
                            }
                            this._context.local.state.put("errored", errored);
                        }.bind(this);

                        fileReader.readAsText(eventData.newValue[0].data); 
                    }
                }
            }.bind(this));*/
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var fileReader: FileReader = new FileReader();
            var errored: boolean = false;
            // TODO: Add event to listen to changes in file
            this._context.local.state.put("latestChangesStep", this._options.gridStep);

            fileReader.onload = function(fileEvent): void
            {
                var file: string = fileEvent.target.result as string;
                var lines: string[] = file.split(/\r?\n/);
                var columns: string[] = lines[0].split(",");

                //Map containing Column Index by Column Name
                //Key -> Column Name
                //Values -> Field Index
                var colToIndex: { [key : string] : number } = {};

                // Check that all mandatory fields are present
                for (let i = 0; i < this._options.mandatoryCols.length; i++)
                {
                    var colIndex: number = columns.indexOf(this._options.mandatoryCols[i]);
                    colToIndex[this._options.mandatoryCols[i]] = colIndex;
                    if (colIndex === -1)
                    {
                        errored = true;
                    }
                }

                for (let i = 0; i < this._options.optionalCols.length; i++)
                {
                    colToIndex[this._options.optionalCols[i]] = columns.indexOf(this._options.optionalCols[i]);
                }

                this._context.local.state.put("errored", errored);

                if (errored)
                {
                    callback(false);
                }
                else
                {
                    // Convert the rows of the CSV file into interest objects
                    var interests: object[] = lines.slice(1, lines.length - 1).map(function(line: string) : object
                    {
                        var interest: { [key : string] : string } = {};
                        var lineColumns: string[] = line.split(/,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/);

                        for (let i = 0; i < this._options.mandatoryCols.length; i++)
                        {
                            interest[this._options.mandatoryCols[i]] = lineColumns[colToIndex[this._options.mandatoryCols[i]]].trim().replace(/(^\"|\"$)/g, "");
                        }
                        for (let i = 0; i < this._options.optionalCols.length; i++)
                        {
                            interest[this._options.optionalCols[i]] = colToIndex[this._options.optionalCols[i]] !== -1 ? lineColumns[colToIndex[this._options.optionalCols[i]]].trim().replace(/(^\"|\"$)/g, "") : null;
                        }

                        return interest;
                    }.bind(this));

                    this._context.local.state.put("interests", interests);

                    callback(true);
                }
            }.bind(this);

            fileReader.readAsText(this._context.local.state.data.interestsFile[0].data);
        }
    }

    ACE.AML.ControllerManager.register("extensions:FileUploadController", FileUploadController, true);
}