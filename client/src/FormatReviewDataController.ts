namespace ACE
{
    var BASE_CONFIG = 
    {
        matchedRecordsData  : "foundRecords.gridData",
        expiredRecordsData  : "expiringInterests",
        filteredMatchedRecordsData : "filteredMatchedRecords",
        filteredExpiredRecordsData : "filteredExpiredRecords",
        filteredNotFoundRecordsData : "filteredNotFoundRecords"
    }

    class FormatReviewDataController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var filteredMatchedRecords : any[] = this._context.local.state.get(this._options.matchedRecordsData)
                .filter(function(record)
            {
                return record._customIsSelected;
            });

            var filteredExpiredRecords : any[] = this._context.local.state.get(this._options.expiredRecordsData)
                .filter(function(record)
            {
                return record._customIsSelected;
            });

            var errorReasons : { [index : number] : string } = this._context.local.state.get("interestIndexToErrorReason"); 
            var interests : any[] = this._context.local.state.get("interests");
            var erroredRecords : any[] = [];

            for (let errorInterestIndex in errorReasons)
            {
                erroredRecords.push(
                    {
                        id : errorInterestIndex,
                        contactName : interests[errorInterestIndex]["Contact Name"],
                        interestSubjectName : interests[errorInterestIndex]["Ticker Name"],
                        errorReason : errorReasons[errorInterestIndex]
                    }
                );
            }

            this._context.local.state.put(this._options.filteredMatchedRecordsData, filteredMatchedRecords);
            this._context.local.state.put(this._options.filteredExpiredRecordsData, filteredExpiredRecords);
            this._context.local.state.put(this._options.filteredNotFoundRecordsData, erroredRecords);

            callback(true);
        }
    }

    ACE.AML.ControllerManager.register("extensions:FormatReviewDataController", FormatReviewDataController, true);
}