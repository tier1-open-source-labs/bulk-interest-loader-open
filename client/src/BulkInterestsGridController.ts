namespace ACE
{
    var BASE_CONFIG = 
    {
        columns             : [],
        dataProvider        : [],
        enableGrouping      : false,
        allowRowSelection   : false,
        groupByField        : "",
        textFilter          : "",
        enableCheckBox      : false,
        enableQuickAdd      : false,
        quickAddConfigPath  : "ACE.Core.QuickAdd.Layouts.QAC",
        enableDeleteRow     : false,
        itemsCounter        : null,
        uncheckableGroups   : [],
        headerRowHeight     : 40,
        gridStep            : 0,
        accountIdParameter  : "entityId",
        relatedDataProvider : ""
    }

    class BulkInterestsGridController
    {
        protected _context          : ACE.AML.ControllerContext;
        protected _options          : any;
        protected _grid             : ACEDataGrid;
        protected _idsToItems       : {[id : string] : any};
        protected _oldRelatedItems  : {[id : string] : any};
        protected _oldDeletedItems  : [];
        protected _accountId        : string;

        protected _updateRelatedDataProvider : Function = function(selectedRowData)
        {
            var altItemIdsToRemove : string[] = [];
            var oldRelatedItemsUpdated : any = this._oldRelatedItems;

            for (let i = 0; i < selectedRowData.length; i++)
            {
                var item : any = selectedRowData[i];
                if (item.altSuggestionId)
                {
                    altItemIdsToRemove.push(item.altSuggestionId);
                }
            }

            var altItemsToAdd : any[] = [];
            var oldRelatedItemsIds : string[] = Object.keys(this._oldRelatedItems);
            for (let i = 0; i < oldRelatedItemsIds.length; i++)
            {
                var itemIndex : number = altItemIdsToRemove.indexOf(oldRelatedItemsIds[i]);
                if (itemIndex === -1)
                {
                    altItemsToAdd.push(this._oldRelatedItems[oldRelatedItemsIds[i]]);
                    var property : string = oldRelatedItemsIds[i];
                    delete oldRelatedItemsUpdated[property];
                }
            }

            var altDataProvider : any[] = this._context.local.state.get(this._options.relatedDataProvider);
            altDataProvider = altDataProvider.filter(function(item)
            {
                var index = altItemIdsToRemove.indexOf(item.id);
                if (index !== -1)
                {
                    oldRelatedItemsUpdated[item.id] = item;
                }
                return index === -1;
            });
            altDataProvider = altDataProvider.concat(altItemsToAdd);
            this._oldRelatedItems = oldRelatedItemsUpdated;

            this._context.local.state.put(this._options.relatedDataProvider, altDataProvider);
        };

        protected _updateStateWithQacResult : Function = function(contactName : string = "")
        {
            // Remove Contact from Contacts Not Found Grid
            var items : any = this._grid.dataProvider.getItems();
            for (let i = 0; i < items.length; i++)
            {
                if (items[i].oldValue === contactName)
                {
                    this._oldDeletedItems.push(items[i]);
                    this._grid.dataProvider.deleteItem(items[i].id);
                    break;                   
                }
            }
            // Remove suggestion from Recommended Matches Grid
            this._updateRelatedDataProvider(this._oldDeletedItems);
        };

        /**
         * Constructor
         * 
         * @param context Context passed in from ACE.AML.DOMUtil.load
         */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
            this._idsToItems = {};
            this._oldRelatedItems = {};
            this._oldDeletedItems = [];
            this._accountId = ACEUtil.getURLParameter(this._options.accountIdParameter);
        }

        /** 
         * Can initialize the controller for business logic that should not be in the constructor
         */
        public init() : void 
        {
            this.createChildren();

            this._context.readyHandler();
        }
        
        public createChildren() : void
        {
            /*var checkBoxValidator : Function = function(item : any)
            {
                return true;
            }.bind(this)*/

            this._grid = new ACEDataGrid( this._context.target,
            {
                //autoHeight              : true, 
                forceFitColumns         : true, 
                explicitInitialization  : true,
                enableColumnReorder     : false,
                allowRowSelection       : this._options.allowRowSelection,
                textFilter              : $(this._options.textFilter).find('input'),
                //fullWidthRows           : true,
                editable                : false,
                enableGroupBy           : this._options.enableGrouping,
                pinGroupByToLeft        : true,
                headerRowHeight         : this._options.headerRowHeight,
                autoEdit                : false,
                //checkBoxSelectorValidationFunction : checkBoxValidator,
                itemRendererFilterFunctions : {}
            });

            // Create and the set the grid columns and any additonal column renderers
            let gridCols : IGridFieldConfig[] = this.addFormattersAndEditors(this._options.columns);
            this._grid.setColumns(gridCols,
            {
                "QuickAddRenderer" : new ItemRenderer(function(row : number, cell : number, value : any, 
                    columnDef : IGridFieldConfig, dataContext:any)
                {
                    return "<button class='quickAddButton quickAddContactBtn' style='height:20px;'/>";     
                }),
                "DeleteRowRenderer" : new ItemRenderer(function(row : number, cell : number, value : any, 
                    columnDef : IGridFieldConfig, dataContext:any)
                {
                    return "<button class='deleteRowButton header-btn-icon header-btn-delete'></button>";
                })
            });

            // Add the data items to the grid and initialize it
            this._grid.addItems(this._options.dataProvider);
            this._grid.grid.init();
            this._grid.grid.invalidate();
            this._grid.grid.resizeCanvas();

            var groupByGetter = function(item : object)
            {
                var mergeStr : string = "{" + this._options.groupByField + "}";
                var mergeVal : string = ACE.StringUtil.mergeParameters(mergeStr, item);
                return mergeVal;
            }.bind(this);
           
            var groupByFormatter = function(aggregate : {value : string, 
                rows : {_customIsSelected : boolean}[]})
            {
                var groupByDiv : string = "<div class='flex-parent is-horizontal' style='align-items:center;'>";       
                var groupByLabel : string = aggregate.value;

                if (!this._options.uncheckableGroups.includes(groupByLabel) && this._options.enableCheckBox)
                {
                    groupByDiv += "<input ";
                    var numItemsChecked : number = aggregate.rows.filter(function(item) 
                    {
                        return item._customIsSelected;
                    }).length;
                    
                    if(numItemsChecked === aggregate.rows.length)
                    {
                        groupByDiv += " checked='checked' ";
                    }
                    else if (numItemsChecked > 0)
                    { 
                        // TODO: Indeterminate checkbox         
                    }

                    groupByDiv += "data='groupByCheckBox' class='groupByCheckBox'" +
                        "type='checkBox' style='margin-right: 5px;'></input>" + groupByLabel;
                }
                else
                {
                    groupByDiv += groupByLabel;
                }

                /*if (this._options.enableDeleteRow)
                {
                    groupByDiv += "<div>" +
                        "<button class='groupByDelete header-btn-icon header-btn-delete'></button></div>";
                }*/

                groupByDiv +=  "</div>";

                return  groupByDiv;
            }.bind(this);

            // Initialize group by rows
            this._grid.dataProvider.setGrouping([
                {
                    getter: groupByGetter,
                    formatter: groupByFormatter,
                    aggregators: [],
                    aggregateCollapsed: false,
                    lazyTotalsCalculation: true
                }
            ]);

            // Update state with the step of the grid that has the latest changes
            // used to ensure that the dataProviders will not recalculate if there are
            // no changes in previous grids
            this._grid.onRowSelectionChange(function(e : any, args : any)
            {
                if (this._options.relatedDataProvider !== "")
                {
                    this._updateRelatedDataProvider(e.data);
                }
                this._context.local.state.put("latestChangesStep", this._options.gridStep);
            }.bind(this));

            // Detect onClick events for groupBy checkboxes, launch QAC buttons, delete row buttons
            this._grid.grid.onClick.subscribe(function(e : any, args : any)
            {
                // Used to detect groupby row checkbox clicks
                var clickedItem : any = args.grid.getDataItem(args.row);

                if ($(e.target).attr("data") == "groupByCheckBox" && clickedItem.groupingKey)
                {
                    // Detect clicks on the groupBy checkboxes and check all the children checkboxes
                    this._grid.dataProvider.beginUpdate();
                    $.each(clickedItem.rows, function(index : number, item : any)
                    {
                        item._customIsSelected = e.target.checked;
                        this._grid.dataProvider.updateItem(item.id, item)
                    }.bind(this));
                    this._grid.dataProvider.endUpdate();

                    this._grid._updateSelectAllCheckBox();
                    if (this._options.relatedDataProvider !== "")
                    {
                        this._updateRelatedDataProvider(this._grid.getSelectedItems());
                    }

                    this._context.local.state.put("latestChangesStep", this._options.gridStep);
                }
                else if ($(e.target).hasClass('quickAddButton'))
                {
                    var contactName : string = clickedItem.oldValue;
                    var nameSplit : string[] = contactName.split(" ");
                    var firstName : string = nameSplit.length >= 1 ? nameSplit[0] : "";
                    var lastName : string = nameSplit.length >= 2 ? nameSplit.splice(1).join() : "";

                    var saveCallback : Function = function(eventData)
                    {
                        this._updateStateWithQacResult(contactName);
                    }.bind(this);

                    ACE.CustomEventDispatcher.trigger("aml-showdialog", 
                    { 
                        "configPath": this._options.quickAddConfigPath, 
                        "isCanvas": true,
                        "state":{
                            "Contact": {
                                "AccountId": this._accountId,
                                "FirstName": firstName,
                                "LastName": lastName
                            },
                            "saveCallback" : saveCallback
                        }
                    });
                }
                else if ($(e.target).hasClass('deleteRowButton'))
                {
                    this._grid.dataProvider.deleteItem(clickedItem.id);
                    if (this._options.itemsCounter)
                    {
                        document.getElementById(this._options.itemsCounter).innerText = this._grid.dataProvider.getItems().length;
                    }
                }
                else if ($(e.target).hasClass('groupByDelete'))
                {
                    this._grid.dataProvider.beginUpdate();
                    $.each(clickedItem.rows, function(index : number, item : any)
                    {
                        this._grid.dataProvider.deleteItem(item.id)
                    }.bind(this));
                    this._grid.dataProvider.endUpdate();

                    if (this._options.itemsCounter)
                    {
                        document.getElementById(this._options.itemsCounter).innerText = this._grid.dataProvider.getItems().length;
                    }
                }
            }.bind(this));  
        }

        /** 
         * Called when DOMUtil determines a resize has occurred and this element is part of the 
         * chain
         */
        public resizeHandler() : void
        {
            if(this._grid)
            {
                this._grid.grid.resizeCanvas();
            }
        }
            
        /** 
         * Called when DOMUtil determines that the element is being shown, can be used to do 
         * resizing (e.g. slickgrid)
         */
        public showHandler() : void 
        {
            if(this._grid)
            {
                this._grid.setOption('textFilter', $(this._options.textFilter).find('input'));
                this._grid.grid.resizeCanvas();
            }
        }

        /** 
         * Cleans up anything required within the controller 
         */
        public destroy() : void {}

        /**
         * Sets focus on the appropriate inner element as needed
         */
        public setFocus() : void {}

        /**
         * Should clear any state properties that this controller manipulates
         */
        public clearState() : void {}

        /**
         * Should reset any state properties that this controller manipulates back 
         * to default values
         */
        public resetState() : void {}

        /**
         * Changes an option dynamically on the fly with the new optionName / value, implementers
         * are responsible for handling any option changes themselves         
         */
        public setOption(optionName : string, value : any) : void
        {
            switch(optionName)
            {
                case "dataProvider" :
                    this._idsToItems = value.reduce(function(accumulator, currentValue)
                    {
                        var id = currentValue.id;
                        accumulator[id] = currentValue;
        
                        return accumulator;
                    }, {});
                    //re-render grid here
                    this._grid.setItems(value);

                    if (this._options.itemsCounter)
                    {
                        document.getElementById(this._options.itemsCounter).innerText = this._grid.dataProvider.getItems().length;
                    }
                    this._grid.grid.invalidate();
                    break;
                case "stepNumber":
                    if (value === 1)
                    {
                        this._oldDeletedItems = [];
                        this._oldRelatedItems = {};
                    }
                    break;
            }

        }

        private addFormattersAndEditors(colsArray : IGridFieldConfig[]) : IGridFieldConfig[]
        {
            //For each FieldType, set the editor and formatter
            $.each(colsArray, function(index : number, column : IGridFieldConfig)
            {
                if(!column.fieldType)
                {
                    //skip to next item if there's no field type
                    return true;
                }

                switch(column.fieldType.toLowerCase())
                {
                    case "lookup":
                        column.editor = Slick.Editors.AutoComplete;
                        break;
                    case "date" :
                        column.editor = Slick.Editors.Date;
                        break;
                    case "picklist" :
                        column.editorDataProvider = column.additionalConfig.dataProvider;
                        column.editor = Slick.Editors.ACEMenuButton;
                        break;
                    case "quickadd":
                        column.formatter = "QuickAddRenderer";
                        break;
                    default:
                        column.editor = Slick.Editors.Text;
                }
            });

            if(this._options.enableCheckBox)
            {       
                // Add a checkbox column         
                let checkBoxCol : IGridFieldConfig =
                {
                    sortable        : false,
                    fieldType       : "CHECKBOX",
                    id              : "checkcolumn",
                    field           : "_customIsSelected",
                    maxWidth        : 30,
                    formatter       : "CheckBoxSelectorItemRenderer",
                    editor          : Slick.Editors.Checkbox,
                    name            : "",
                    order           : this._options.undoColumnPosition,
                    defaultValue    : null,
                    defaultId       : null,
                    visible         : true,
                    sObjectName     : null,
                    picklistOptions : null,
                    disabled        : false,
                    placeHolder     : null,
                    innerCSS        : null,
                    hoverText       : null,
                    minWidth        : 30,
                    additionalConfig : null,
                    editorDataProvider : null
                };
                colsArray.unshift(checkBoxCol);
            }

            if (this._options.enableQuickAdd)
            {
                let quickAddColumn: IGridFieldConfig =
                {
                    id              : "quickAddColumn",
                    field           : "_customWasAdded",
                    sortable        : true,
                    maxWidth        : 30,
                    formatter       : "QuickAddRenderer",
                    fieldType       : "QUICKADD",
                    name            : "",
                    order           : this._options.undoColumnPosition,
                    defaultValue    : null,
                    defaultId       : null,
                    visible         : true,
                    sObjectName     : null,
                    picklistOptions : null,
                    disabled        : false,
                    placeHolder     : null,
                    innerCSS        : null,
                    hoverText       : null,
                    editor          : null,
                    minWidth        : 30,
                    additionalConfig : null,
                    editorDataProvider : null
                };
                colsArray.push(quickAddColumn);
            }

            if (this._options.enableDeleteRow)
            {
                // Add a column containing delete buttons for the corresponding rows
                let deleteRowColumn: IGridFieldConfig =
                {
                    id              : "deleteRowColumn",
                    field           : "_deleted",
                    sortable        : true,
                    maxWidth        : 30,
                    formatter       : "DeleteRowRenderer",
                    fieldType       : "",
                    name            : "",
                    order           : this._options.undoColumnPosition,
                    defaultValue    : null,
                    defaultId       : null,
                    visible         : true,
                    sObjectName     : null,
                    picklistOptions : null,
                    disabled        : false,
                    placeHolder     : null,
                    innerCSS        : null,
                    hoverText       : null,
                    editor          : null,
                    minWidth        : 30,
                    additionalConfig : null,
                    editorDataProvider : null
                };
                colsArray.push(deleteRowColumn);
            }

            return colsArray;
        };
    }

    // this registers the controller to be available for use within the AML framework
    // the registration name can be anything
    // the last parameter determines if its the "base" controller for that registration name
    // if another class registers the same registration name with the last parameter being false
    // when AML loads the controller the second one will win
    ACE.AML.ControllerManager.register("extensions:BulkInterestsGridController", BulkInterestsGridController, false);
}