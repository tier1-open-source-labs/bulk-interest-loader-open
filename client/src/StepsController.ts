namespace ACE
{
    class StepsController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;
        private __stepNumber: number;

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = context.options;
            this.__stepNumber = 1;
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
          this.__stepNumber = this._context.local.state.get("stepNumber");
            switch (e.type) {
              case this._options.prevStepEvent:
                if (this.__stepNumber > 1) 
                {
                  this.__stepNumber -= 1;
                  this._context.local.state.put("stepNumber", this.__stepNumber);
                  this._context.local.eventDispatcher.trigger(
                    "startStep" + this.__stepNumber
                  );
                }
                break;
              case this._options.nextStepEvent:
                if (this.__stepNumber < 5) 
                {
                  this.__stepNumber += 1;
                  this._context.local.state.put("stepNumber", this.__stepNumber);
                  this._context.local.eventDispatcher.trigger(
                    "startStep" + this.__stepNumber
                  );
                }
                break;
            }
            
            callback(true);
        }
    }

    ACE.AML.ControllerManager.register("extensions:StepsController", StepsController, true);
}