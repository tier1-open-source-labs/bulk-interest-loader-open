/**
 * Name: ExtensionsAMLLoader.js 
 * Description: Short javascript that tells the window to import
 *              an Ellipsis layout into the form
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */
(function($)
{
    var path              = ACEUtil.getURLParameter('layoutPath');
    //var controllerClass = ACEUtil.getURLParameter('apexClass');
    //var userId          = ACEUtil.getURLParameter('userId');

    state = {};

    if(ACE.AML)
    {
        this.load = ACE.AML.DOMUtil.load("{@" + path + ".Layout}", "body", state, false, null);
    }
//# sourceURL=ExtensionsAMLLoader.js
})(jQuery);