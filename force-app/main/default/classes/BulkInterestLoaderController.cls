/**
 * Name: BulkInterestLoaderController.js 
 * Description: Back End controller for the Bulk Interest Loader
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */
public with sharing class BulkInterestLoaderController
{
    public static final String BULK_INTEREST_LOADER_FEATURE_PATH = 'ACE.Extensions.BulkInterestLoader';
    private static final Integer THRESHOLD_SMALL = Integer.valueOf(T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), BULK_INTEREST_LOADER_FEATURE_PATH, 'DistanceSmall'));
    private static final Integer THRESHOLD_MEDIUM = Integer.valueOf(T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), BULK_INTEREST_LOADER_FEATURE_PATH, 'DistanceMedium'));
    private static final String INTEREST_SUBJECT_TYPE = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), BULK_INTEREST_LOADER_FEATURE_PATH, 'InterestSubjectType');
    private static final String INTEREST_REASON = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), BULK_INTEREST_LOADER_FEATURE_PATH, 'InterestReason');
    private static final String INTEREST_SOURCE = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), BULK_INTEREST_LOADER_FEATURE_PATH, 'InterestSource');
    private static final Integer QUERY_RECORD_CAP = 10000;

    //  ==================================================================
    //  Method: getNames
    //  Description: Loops through a list of SObjects and returns a list of their Names.
    //  Params: objects - List of SObjects to be looped through.
    //  ==================================================================
    private static String[] getNames(SObject[] objects)
    {
        String[] names = new List<String>();
        
        for (SObject o : objects)
        {
            names.add((String)o.get('Name'));
        }

        return names;
    }

    //  ==================================================================
    //  Method: findClosestSObject
    //  Description: Will iterate through a list of objects, comparing its fields to corresponding
    //               record values. Each comparison will use a threshold of the same index from the 
    //               threshold list. Returns the index of the SObject containing the closest match. 
    //  Params: recordValues - A list of values that will be compared
    //          objects - A list of SObjects from which the strings to compare to will be extracted
    //          fields - The fields in the SObject that will be compared with recordValues
    //          thresholds - A list of max Levenshtein distances for each comparison
    //  ==================================================================
    private static Integer[] findClosestSObject(String[] recordValues, SObject[] objects, String[] fields, Integer[] thresholds, Integer prevLeastDistance)
    {
        Integer leastDistance = prevLeastDistance;
        Integer suggestionIndex = -1;

        for (Integer i = 0; i < objects.size(); i++)
        {
            SObject compareObject = objects[i];

            for (Integer j = 0; j < fields.size(); j++)
            {
                String compareString = (String)compareObject.get(fields[j]);

                if (recordValues[j] != null && compareString != null)
                {
                    Integer currentDistance = recordValues[j].getLevenshteinDistance(compareString);

                    if (currentDistance < leastDistance && currentDistance <= thresholds[j])
                    {
                        leastDistance = currentDistance;
                        suggestionIndex = i;
                    }
                }
            }
        }

        return new List<Integer>{suggestionIndex, leastDistance};
    }

    //  ==================================================================
    //  Method: matchInterests
    //  Description: Processes a list of input interests (stored as a Map of fields -> values)
    //      and returns a list of interests that match those stored in the database. 
    //  Params: interests - A List of Maps where each Map represents an interest.
    //          accountId - The Id of the account used for searching for Contacts.
    //  ==================================================================
    @ReadOnly
    @RemoteAction 
    public static List<Object> matchInterests(List<Map<String, String>> interests, Id accountId, 
        String contactNameField, String tickerNameField, String companyNameField, String contactEmailField,
        Map<String, Suggestion> prevSuggestions, Id lastQueriedId)
    {
        List<String> contactNames = new List<String>();
        List<String> tickerNames = new List<String>();
        for (Map<String,String> interest : interests)
        {
            String contactName = interest.get(contactNameField);
            String tickerName = interest.get(tickerNameField);

            contactNames.add(contactName);
            tickerNames.add(tickerName);
        }

        //Map containing Suggestions by old value (value with a suggested change)
        //Key -> Old Value
        //Values -> Suggestion for a change to that value
        Map<String, Suggestion> suggestions = new Map<String, Suggestion>();

        // Find all Contacts of the target Account
        Contact[] accountContacts = [SELECT Name, Email 
                                       FROM Contact 
                                      WHERE AccountId = :accountId
                                        AND T1C_Base__Inactive__c = false];
        String[] accountContactNames = getNames(accountContacts);

        // Find all Interests of Subject Type Ticker
        T1C_Base__Interest_Subject__c[] allInterests = [SELECT Id, Name, T1C_Base__Description__c  
                                                          FROM T1C_Base__Interest_Subject__c 
                                                         WHERE T1C_Base__Interest_Subject_Type__r.Name = :INTEREST_SUBJECT_TYPE
                                                           AND Id > :lastQueriedId
                                                      ORDER BY Id
                                                         LIMIT :QUERY_RECORD_CAP];
        Id newLastQueriedId = lastQueriedId;
        if (allInterests.size() > 0)
        {
        	newLastQueriedId = (Id) allInterests[allInterests.size() - 1].Id;
        }
 
        String[] allInterestNames = getNames(allInterests);

        T1C_Base__Interest_Subject__c[] matchedInterests = [SELECT Id, Name, T1C_Base__Description__c, T1C_Base__Interest_Subject_Type__c 
                                                          FROM T1C_Base__Interest_Subject__c 
                                                         WHERE T1C_Base__Interest_Subject_Type__r.Name = :INTEREST_SUBJECT_TYPE
                                                           AND Name IN :tickerNames];
        String[] matchedInterestNames = getNames(matchedInterests);
        

        // Find Matches
        for (Integer i = 0; i < interests.size(); i++)
        {
            String contactName = interests[i].get(contactNameField);
            String tickerName = interests[i].get(tickerNameField);
            String companyName = interests[i].get(companyNameField);
            String contactEmail = interests[i].get(contactEmailField);

            // No Suggestion has been made yet
            if (allInterests.size() < QUERY_RECORD_CAP && !suggestions.containsKey(contactName))
            {
                // Contact name does not exist and we are on the final interation of the remoting call
                if (!accountContactNames.contains(contactName))
                {
                    Integer prevLeastDistance = 10;
                    if (prevSuggestions.containsKey(contactName))
                    {
                        prevLeastDistance = prevSuggestions.get(contactName).matchDistance;
                    }
                    
                    // Check for similar contact names and make recommendations
                    Integer [] matchingResult = findClosestSObject(new List<String>{contactName, contactEmail}, 
                        accountContacts, new List<String>{'Name', 'Email'}, 
                        new List<Integer>{THRESHOLD_MEDIUM, THRESHOLD_MEDIUM}, prevLeastDistance);

                    Integer closestContactIndex = matchingResult[0];
                    Integer closestMatchDistance = matchingResult[1];
                    
                    if (closestContactIndex != -1)
                    {
                        // A better match was found
                        String suggestedContactName = (String)accountContacts[closestContactIndex].get('Name');
                        suggestions.put(contactName, new Suggestion('Contact Name', suggestedContactName, 
                            null, i, 'RENAME', closestMatchDistance));
                    }
                    else if (prevSuggestions.containsKey(contactName))
                    {
                        // We did not find a better match but a match was found on a previous remote call
                        suggestions.put(contactName, prevSuggestions.get(contactName));
                    }
                    else if (allInterests.size() < QUERY_RECORD_CAP)
                    {
                        // No similar Contacts found and this is the last remote call so we will suggest 
                        // creating a new Contact
                        suggestions.put(contactName, new Suggestion('Contact Name', contactName, null, i, 'CREATE', 0));
                    }
                }
            }
            else if (suggestions.containsKey(contactName))
            {
                suggestions.get(contactName).interestIndices.add(i);
            }

            // No Suggestion has been made yet
            if (!suggestions.containsKey(tickerName))
            {
                // Ticker name does not exist
                if (!matchedInterestNames.contains(tickerName))
                {
                    Integer prevLeastDistance = 10;
                    if (prevSuggestions.containsKey(tickerName))
                    {
                        prevLeastDistance = prevSuggestions.get(tickerName).matchDistance;
                    }

                    // Find similar Interest Subject Names to Ticker Name
                    Integer [] matchingResult = findClosestSObject(new List<String>{tickerName, companyName}, 
                        allInterests, new List<String>{'Name', 'T1C_Base__Description__c'}, 
                        new List<Integer>{THRESHOLD_SMALL, THRESHOLD_MEDIUM}, prevLeastDistance);
                    
                    Integer closestInterestIndex = matchingResult[0];
                    Integer closestMatchDistance = matchingResult[1];

                    if (closestInterestIndex != -1)
                    {
                        String suggestedTickerName = (String)allInterests[closestInterestIndex].get('Name');
                        suggestions.put(tickerName, new Suggestion('Ticker Name', suggestedTickerName, 
                            (String)allInterests[closestInterestIndex].get('T1C_Base__Description__c'), i, 'RENAME', closestMatchDistance));
                    }
                    else if (prevSuggestions.containsKey(tickerName))
                    {
                        // We did not find a better match but a match was found on a previous remote call
                        suggestions.put(tickerName, prevSuggestions.get(tickerName));
                    }
                }
            }
            else 
            {
                suggestions.get(tickerName).interestIndices.add(i);               
            }
        }
        return new List<Object>{suggestions, newLastQueriedId, allInterests.size()};
    }

    //  ==================================================================
    //  Method: getExpiringInterests
    //  Description: Gets a list of Interest records that are not in the map for all contacts in the map
    //
    //  Params: interests - A List of Maps where each Map represents an input interest record.
    //          accountId - The Id of the account used for searching for Contacts.
    //          contactIdField - The field on the Input interest object to get the intended Contact Id from
    //          intSubIdField - The field on the Input interest object to get the intended Interest Subject Id from
    //  ==================================================================
    @RemoteAction
    public static List<InterestRecord> getExpiringInterests(List<Map<String, String>> interests, 
        List<Id> contactIdsList, Id accountId, String contactIdField, String intSubIdField)
    {
        Set<Id> contactIds = new Set<Id>(contactIdsList);
        Set<Id> intSubIds = new Set<Id>();

        //Map of Contact Id:Interest Id to Boolean.
        //Marks which contact and interest subject combinations are being created so we can mark the others for expiry
        Map<String, Boolean> nonExpiringInterests = new Map<String, Boolean>();

        for(Map<String, String> interestRecord : interests)
        {
            String contId = interestRecord.get(contactIdField);
            String intSubId = interestRecord.get(intSubIdField);

            if(intSubId != null)
            {
                intSubIds.add(intSubId);
            }

            nonExpiringInterests.put(contId + ':' + intSubId, true);
        }

        String expQuery = 'SELECT Id,' +
                                 'T1C_Base__Contact__c,' +
                                 'T1C_Base__Contact__r.Name,' +
                                 'T1C_Base__Interest_Subject__c,' +
                                 'T1C_Base__Interest_Subject__r.Name,' +
                                 'T1C_Base__Interest_Subject__r.T1C_Base__Description__c' +
                           ' FROM T1C_Base__Interest__c' +
                          ' WHERE T1C_Base__Contact__c IN :contactIds' +
                            ' AND T1C_Base__Expired__c = FALSE' +
                            ' AND T1C_Base__Interest_Subject__r.T1C_Base__Interest_Subject_Type__r.Name = :INTEREST_SUBJECT_TYPE';

        //Uncomment once CC is updated
        /*ACE.DatabaseOptions opts = new ACE.DatabaseOptions(new Map<String,Object>{'contactIds'=>contactIds, 'intSubIds'=>intSubIds});
        T1C_Base__Interest__c[] expInts = (T1C_Base__Interest__c[])ACE.Database.query(expQuery ,opts);*/

        T1C_Base__Interest__c[] expInts = Database.query(expQuery);

        InterestRecord[] retList = new InterestRecord[]{};

        for(T1C_Base__Interest__c curInt : expInts)
        {
            String contIntSubKey = curInt.T1C_Base__Contact__c + ':' + curInt.T1C_Base__Interest_Subject__c;
            if(nonExpiringInterests.get(contIntSubKey) != null)//If we submitted this contact and interest subject combination, we should keep it
            {
                continue;
            }

            retList.add(new InterestRecord(curInt));//Else we return it as an expiring interest record
        }

        return retList;
    }
    //  Method: getMatchedRecords
    //  Description: Processes a list of input interests (stored as a Map of fields -> values)
    //      and returns a list of Interests, an Interest is created when both the Contact and
    //      the Interest Subject Name exist. 
    //  Params: interests - A List of Maps where each Map represents an interest.
    //          accountId - The Id of the account used for searching for Contacts.
    //  ==================================================================
    @RemoteAction
    public static OutputInterestsAndErrors getMatchedRecords(List<Map<String,String>> interests, Id accountId, 
        String contactNameField, String tickerNameField, Map<String, String> errorToErrorAlias)
    {
        List<String> contactNames = new List<String>();
        List<String> tickerNames = new List<String>();
        for (Map<String,String> interest : interests)
        {
            String contactName = interest.get(contactNameField);
            String tickerName = interest.get(tickerNameField);

            contactNames.add(contactName);
            tickerNames.add(tickerName);
        }

        Contact[] accountContacts = [SELECT Id, Name
                                       FROM Contact 
                                      WHERE AccountId = :accountId
                                        AND Name IN :contactNames
                                        AND T1C_Base__Inactive__c = false];

        //Map containing Contacts by their Name
        //Key -> Name
        //Values -> Contact
        Map<String, Contact> namesToContacts = new Map<String,Contact>();
        for (Contact contact : accountContacts)
        {
            namesToContacts.put((String)contact.get('Name'), contact);
        }

        T1C_Base__Interest_Subject__c[] allInterests = [SELECT Id, Name, T1C_Base__Description__c, T1C_Base__Interest_Subject_Type__c 
                                                          FROM T1C_Base__Interest_Subject__c 
                                                         WHERE T1C_Base__Interest_Subject_Type__r.Name = :INTEREST_SUBJECT_TYPE
                                                           AND Name IN :tickerNames];
        
        //Map containing Interest Subjects by their Name
        //Key -> Name
        //Values -> T1C_Base__Interest_Subject__c
        Map<String, T1C_Base__Interest_Subject__c> namesToInterestSubjects = new Map<String,T1C_Base__Interest_Subject__c>();
        for (T1C_Base__Interest_Subject__c interestSubject : allInterests)
        {
            namesToInterestSubjects.put((String)interestSubject.get('Name'), interestSubject);
        }

        //Map containing Interests by Contact Name
        //Key -> Contact Name
        //Values -> All Interests of the Contact
        Map<String, List<T1C_Base__Interest__c>> contactsToInterests = new Map<String, List<T1C_Base__Interest__c>>();
        for (T1C_Base__Interest__c interest : [SELECT Id, T1C_Base__Contact__r.Name, T1C_Base__Interest_Subject_Name__c 
                                                 FROM T1C_Base__Interest__c
                                                WHERE T1C_Base__Interest_Subject_Type__r.Name = :INTEREST_SUBJECT_TYPE
                                                  AND T1C_Base__Contact__c IN :accountContacts
                                                  AND T1C_Base__Interest_Subject_Name__c IN :tickerNames]) // TODO: Dont use this one
        {
            String interestContactName = interest.T1C_Base__Contact__r.Name;

            if (contactsToInterests.containsKey(interestContactName))
            {
                contactsToInterests.get(interestContactName).add(interest);
            }
            else
            {
                contactsToInterests.put(interestContactName, new List<T1C_Base__Interest__c>{interest});
            }
        }

                                       
        OutputInterest[] matchedRecords = new List<OutputInterest>();
        Map<Integer, String> interestIndexToErrorReason = new Map<Integer,String>();
        // For Checking duplicates
        Map<String, List<String>> contactsToNewInterests = new Map<String, List<String>>();

        for (Integer i = 0; i < interests.size(); i++)
        {
            String contactName = interests[i].get(contactNameField);
            String tickerName = interests[i].get(tickerNameField);

            if (!contactsToNewInterests.containsKey(contactName) || !contactsToNewInterests.get(contactName)
                .contains(tickerName))
            {
                if (namesToContacts.containsKey(contactName))
                {
                    if (namesToInterestSubjects.containsKey(tickerName))
                    {
                        Contact contact = namesToContacts.get(contactName);
                        T1C_Base__Interest_Subject__c interestSubject = namesToInterestSubjects.get(tickerName);
                        
                        Id contactId = (Id) contact.get('Id');
                        Id interestSubjectId = (Id) interestSubject.get('Id');
                        Id interestSubjectTypeId = (Id) interestSubject.get('T1C_Base__Interest_Subject_Type__c');
                        String interestSubjectName = (String) interestSubject.get('Name');
                        String interestSubjectDescription = (String) interestSubject.get('T1C_Base__Description__c');

                        T1C_Base__Interest__c interest = new T1C_Base__Interest__c(T1C_Base__Account__c=accountId, 
                            T1C_Base__Contact__c=contactId, 
                            T1C_Base__Interest_Subject__c=interestSubjectId,
                            T1C_Base__Interest_Subject_Type__c=interestSubjectTypeId,
                            T1C_Base__Reason__c=INTEREST_REASON,//'Interest',
                            T1C_Base__Source__c=INTEREST_SOURCE,//'Focus List',
                            T1C_Base__Expired__c=false,
                            T1C_Base__Last_Discussed__c = System.today());

                        // If this interest already exist, find and add its Id
                        if (contactsToInterests.containsKey(contactName))
                        {
                            List<T1C_Base__Interest__c> contactInterests = contactsToInterests.get(contactName);
                            Boolean alreadyHasInterest = false;
                            
                            for (T1C_Base__Interest__c contactInterest : contactInterests)
                            {
                                if (contactInterest.T1C_Base__Interest_Subject_Name__c.equals(tickerName))
                                {
                                    interest.put('Id', contactInterest.Id);
                                    alreadyHasInterest = true;
                                    break;
                                }
                            }

                            if (!alreadyHasInterest)
                            {
                                interest.put('T1C_Base__As_Of_Date__c', System.today());
                            }                                 
                        }
                        else 
                        {
                            interest.put('T1C_Base__As_Of_Date__c', System.today());                      
                        }

                        OutputInterest outputInterest = new OutputInterest(interest, contactName, interestSubjectName, interestSubjectDescription);

                        matchedRecords.add(outputInterest);
                    }
                    else 
                    {
                        interestIndexToErrorReason.put(i, errorToErrorAlias.get('ERROR_TICKER_NAME_MISSING'));
                    }
                }
                else 
                {
                    interestIndexToErrorReason.put(i, errorToErrorAlias.get('ERROR_CONTACT_NAME_MISSING'));                
                }

                // Add the interest to map so we can check for duplicates later
                if (contactsToNewInterests.containsKey(contactName))
                {
                    contactsToNewInterests.get(contactName).add(tickerName);
                }
                else 
                {
                    contactsToNewInterests.put(contactName, new List<String>{tickerName});    
                }
            }
            else 
            {
                // Duplicate records can be handled here
            }
        }
        return new OutputInterestsAndErrors(matchedRecords, interestIndexToErrorReason);
    }

    //  ==================================================================
    //  Method: getGridConfigs
    //  Description: Gets the grid configs for the input AFR paths
    //
    //  Params: stateToPathMap - a map containing the statefield as the key and the AFR path as the value
    //                           the path is expected to have subfeatures which define each individual Column                                
    //  ==================================================================
    @RemoteAction
    public static Map<String, GridFieldConfig[]> getGridConfigs(Map<String, String> stateToPathMap, String basePath)
    {
        T1C_FR.Feature parentFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId() , basePath);

        Map<String, GridFieldConfig[]> colsMap = new Map<String, GridFieldConfig[]>();

        System.debug('Found AFR paths: '+ parentFeature.SubFeatMap.keySet());

        for(String stateField : stateToPathMap.keySet())
        {
            String afrPath = stateToPathMap.get(stateField);
            GridFieldConfig[] gridFields = new GridFieldConfig[]{};

            //The subfeatures of the AFR 
            T1C_FR.Feature configParent = parentFeature.getFeature(afrPath);

            /*if(configParent == null)
            {
                continue;
            }*/

            for(T1C_FR.Feature columnFeat : configParent.SubFeatures)
            {
                gridFields.add(new GridFieldConfig(columnFeat));
            }

            colsMap.put(stateField, gridFields);
        }

        return colsMap;
    }

    //  ==================================================================
    //  Method: getGridConfigs
    //  Description: Gets the grid configs for the input AFR paths
    //
    //  Params: expiredInterests - a list of interests that are set to expire. Input is expected to be formal Interest records
    //                             with proper Ids
    //  ==================================================================
    @RemoteAction
    public static void expireInterests(List<T1C_Base__Interest__c> expiredInterests)
    {
        try
        {
            for(T1C_Base__Interest__c curInterest : expiredInterests)
            {
                curInterest.T1C_Base__Expired__c = true;
                //curInterest.T1C_Base__As_Of_Date__c = System.today();
                //curInterest.T1C_Base__Last_Discussed__c = System.today();
            }
    
            update expiredInterests;//Making this an update to force Ids in the input
        }        
        catch(DmlException ex)
        {   
        
        }
    }

    //  ==================================================================
    //  Method: getGridConfigs
    //  Description: Gets the grid configs for the input AFR paths
    //
    //  Params: expiredInterests - a list of interests that are set to expire. Input is expected to be formal Interest records
    //                             with proper Ids
    //  ==================================================================
    @RemoteAction
    public static SaveResult[] saveInterests(List<T1C_Base__Interest__c> interestsToSave)
    {
        SaveResult[] results = new SaveResult[]{};
        try{
            upsert interestsToSave;

            for(T1C_Base__Interest__c curInt : interestsToSave)
            {
                results.add(new SaveResult(curInt, curInt.Id, true));
            }

            return results;
        } 
        catch(DmlException ex)
        {   
            for(Integer i = 0 ; i<ex.getNumDml(); i++)
            {
                //ex.getDmlIndex(i) gets the index of the object in the original list
                results.add(new SaveResult(interestsToSave.get( ex.getDmlIndex(i) ), 'Exception' + i, false, ex.getDmlMessage(i)));
            }

            return results;
        }
    }

    public class SaveResult
    {
        public Boolean status;
        public Sobject data;
        public String id;
        public string message;

        public SaveResult(SObject inData, String inputId, Boolean status)
        {
            this.data = inData;
            this.id = inputId;
            this.status = status;
        }

        public SaveResult(SObject inData, String inputId, Boolean status, String message)
        {
            this.data = inData;
            this.id = inputId;
            this.status = status;
            this.message = message;
        }
    }

    /** 
     * Name: Suggestion
     * Description: Class that stores information related to Contact/Ticker Name
     *              that was not successfully matched and the affected rows in
     *              the CSV file.
     **/
    public class Suggestion implements Comparable
    {
        // Column name
        public String field;

        // Suggested new value
        public String newValue;

        // Any extra value (such as a discription)
        public String extraValue;

        // RENAME or CREATE (No similar contacts were found, suggest creating a new one)
        public final String type;
        
        // Indices into the interests data list (extracted from the CSV file), will
        // be used to update the data before moving onto step 3
        public Integer[] interestIndices;

        // Distance from old value to suggested new value
        public Integer matchDistance;

        public Suggestion(String field, String newValue, String extraValue, Integer index, String type, Integer matchDistance)
        {
            this.field = field;
            this.newValue = newValue;
            this.extraValue = extraValue;
            this.interestIndices = new List<Integer>{index};
            this.type = type;
            this.matchDistance = matchDistance;
        }

        public Integer compareTo(Object compareTo)
        {
            Suggestion target = (Suggestion)compareTo;
            return this.field.compareTo(target.field);
        }
    }

    //Private class used to make the data in the grids compatible with the ALM.DataGrid 
    //definition of the columns, which always have a field prefix of "data."
    public class InterestRecord
    {
        public T1C_Base__Interest__c data;
        public String id;

        public InterestRecord(T1C_Base__Interest__c inData)
        {
            this.data = inData;
            this.id = inData.Id;
        }
    }

    /** 
     * Name: OutputInterest
     * Description: Stores an Interest SObject alongside additional fields that are
     *              displayed on the frontend.
     **/
    public class OutputInterest 
    {
        public T1C_Base__Interest__c interest;
        public String contactName;
        public String interestSubjectName;
        public String interestSubjectDescription;

        public OutputInterest(T1C_Base__Interest__c interest, String contactName, String interestSubjectName,
            String interestSubjectDescription)
        {
            this.interest = interest;
            this.contactName = contactName;
            this.interestSubjectName = interestSubjectName;
            this.interestSubjectDescription = interestSubjectDescription;
        }
    }

    /** 
     * Name: OutputInterest
     * Description: Stores a list of OutputInterests for matched records, and a Map
     *              of record index -> error reason for not matched records.
     **/
    public class OutputInterestsAndErrors
    {
        public List<OutputInterest> interests;
        public Map<Integer, String> interestIndexToErrorReason;

        public OutputInterestsAndErrors(List<OutputInterest> interests, 
            Map<Integer, String> interestIndexToErrorReason)
        {
            this.interests = interests;
            this.interestIndexToErrorReason = interestIndexToErrorReason;
        }
    }

    /** 
     * Name: GridFieldConfig
     * Description: Wrapper Class for each Grid Field config.
     **/
    public class GridFieldConfig implements Comparable
    {
        public string id;
        public string fieldType;
        public Integer minWidth;
        public Integer width;
        public string name;
        public Integer order;
        public string defaultValue;
        public string defaultId;
        public boolean visible;
        public String sObjectName;
        public String field;
        public list<string> picklistOptions;
        public Object additionalConfig;
        public String parentSObject;
        public String parentSObjectField;
        public Boolean disabled;
        public String placeHolder;
        public String innerCSS;//Used for formatting icon columns
        public String hoverText;
        
        public GridFieldConfig(T1C_FR.Feature inputFeature)
        {
            system.debug('Feature: ' + inputFeature.Name);
            this.id                 = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, inputFeature.Name);
            this.fieldType          = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'FieldType');
            this.minWidth           = Integer.valueOf( T1C_FR.FeatureCacheWebSvc.getAttributeDflt(UserInfo.getUserId(), inputFeature.Name, 'Width', '100') );
            this.width              = Integer.valueOf( T1C_FR.FeatureCacheWebSvc.getAttributeDflt(UserInfo.getUserId(), inputFeature.Name, 'Width', '100') );
            this.name               = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'Label');
            this.order              = Integer.valueOf( T1C_FR.FeatureCacheWebSvc.getAttributeDflt(UserInfo.getUserId(), inputFeature.Name, 'Order', '0') );
            this.parentSObjectField = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'ParentSObjectField');
            this.sObjectName        = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'SObjectName');
            this.field              = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'DataField');
            this.visible            = Boolean.valueOf( T1C_FR.FeatureCacheWebSvc.getAttributeDflt(UserInfo.getUserId(), inputFeature.Name, 'Visible','true') );
            this.disabled           = Boolean.valueOf( T1C_FR.FeatureCacheWebSvc.getAttributeDflt(UserInfo.getUserId(), inputFeature.Name, 'Disabled','false') );
            this.placeHolder        = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'PlaceHolder');
            this.defaultValue       = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'DefaultValue');
            this.innerCSS           = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), inputFeature.Name, 'InnerCSS');
            
            //set up the additionalConfig properties to have the autocomplete to work
            if(this.fieldType == 'LOOKUP')
            {
            SObjectUtil.DynamicUILookupConfig o = new SObjectUtil.DynamicUILookupConfig( AFRUtil.getSubfeature(inputFeature, 'FieldConfig') );
            this.additionalConfig = new SObjectUtil.DynamicUILookupConfig();
            this.additionalConfig = o;
            }//field type LOOKUP
            else if(this.fieldType == 'PICKLIST')
            {
            SObjectUtil.DynamicUIPicklistConfig o = new SObjectUtil.DynamicUIPicklistConfig();
            list<string> pickListValues = o.getPicklistValues(this.sObjectName, this.field);
            o.dataProvider = pickListValues;
            this.defaultValue = o.defaultValue;
            //instantiate additonalConfig as DynamicUIPicklistConfig
            this.additionalConfig = new SObjectUtil.DynamicUIPicklistConfig();
            this.additionalConfig = o;
            }
        }//public GridFieldConfig(T1C_FR.Feature inputFeature)
        

        public Integer compareTo(Object compareTo) {
        
            if (this.order == ( (GridFieldConfig)compareTo ).order) return 0;
            
            if (this.order > ( (GridFieldConfig)compareTo ).order) return 1;
            
            return -1;       
        
        }
    }

}