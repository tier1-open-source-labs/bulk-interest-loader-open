@isTest
public class TestBulkInterestLoaderController
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    private static void setAFRs()
    {
        T1C_FR__Feature__c aceFeature     	= new T1C_FR__Feature__c(Name = 'ACE',            		T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c extensions     	= new T1C_FR__Feature__c(Name = 'Extensions',     		T1C_FR__Name__c = 'ACE.Extensions');
        T1C_FR__Feature__c bulkInterestLoader = new T1C_FR__Feature__c(Name = 'BulkInterestLoader', 	T1C_FR__Name__c = 'ACE.Extensions.BulkInterestLoader');
        
        upsert new list<T1C_FR__Feature__c>
        {
            aceFeature,
            extensions,
            bulkInterestLoader
        };        
        
        insert new list<T1C_FR__Attribute__c>
        {    
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = bulkInterestLoader.Id, Name = 'DistanceSmall', 	T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = bulkInterestLoader.Id, Name = 'DistanceMedium', 	T1C_FR__Value__c = '5')
        }; 
    }

    @isTest static void TestMatchInterests()
    {
        // Set up Test Database
        setAFRs();
        
        Account testAccount = dataFactory.makeAccount('Duck Land');
        insert testAccount;
        
        Contact testContact = dataFactory.makeContact(testAccount.Id, 'Duck', 'Man');
        insert testContact;
        
		T1C_Base__Interest_Subject_Type__c testInterestSubjectType = dataFactory.makeInterestSubjectType('Ticker', null);
        insert testInterestSubjectType;
        
       	T1C_Base__Interest_Subject__c testInterestSubject = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK', null);
        insert testInterestSubject;

        // Test Data
        Map<String, String> interestWithContactNameTypo = new Map<String, String>{'Contact Name' => 'Dukk Mnn', 'Ticker Name' => 'DUCK'};
        Map<String, String> interestWithTickerNameTypo = new Map<String, String>{'Contact Name' => 'Duck Man', 'Ticker Name' => 'DUC'};
        Map<String, String> missingInterest = new Map<String, String>{'Contact Name' => 'asdasf', 'Ticker Name' => 'asfasf'};

        List<Map<String, String>> testInterests = new List<Map<String, String>>{interestWithContactNameTypo, interestWithTickerNameTypo, missingInterest};      
        String contactNameField = 'Contact Name';
        String tickerNameField = 'Ticker Name';
        String companyNameField = 'Company Name';
        String contactEmailField = 'Contact Email';
        Map<String, BulkInterestLoaderController.Suggestion> prevSuggestions = new Map<String, BulkInterestLoaderController.Suggestion>();
        Id lastQueriedId = '000000000000000000';

        // Perform test
        Test.startTest();
        List<Object> matchInterestsResult = BulkInterestLoaderController.matchInterests(testInterests, testAccount.Id, contactNameField, tickerNameField, companyNameField, contactEmailField, prevSuggestions, lastQueriedId);        
        Test.stopTest();
        
        System.debug(matchInterestsResult);
        
        Map<String, BulkInterestLoaderController.Suggestion> expectedSuggestions = new Map<String, BulkInterestLoaderController.Suggestion>
        {
            'Dukk Mnn' => new BulkInterestLoaderController.Suggestion('Contact Name', 'Duck Man', null, 0, 'RENAME', 2),
            'DUC'	   => new BulkInterestLoaderController.Suggestion('Ticker Name', 'DUCK', null, 0, 'RENAME', 1)
        };
            
        System.assert(((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).containsKey('Dukk Mnn'));
        System.assert(((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).containsKey('DUC'));
        System.assertEquals(expectedSuggestions.get('Dukk Mnn').newValue, ((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).get('Dukk Mnn').newValue);
        System.assertEquals(expectedSuggestions.get('DUC').newValue, ((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).get('DUC').newValue);     
    }
    
    @isTest static void TestMatchInterestsWithPrevSuggestions()
    {
		// Set up Test Database
        setAFRs();
        
        Account testAccount = dataFactory.makeAccount('Duck Land');
        insert testAccount;
        
        Contact testContact = dataFactory.makeContact(testAccount.Id, 'Duck', 'Man');
        insert testContact;
        
		T1C_Base__Interest_Subject_Type__c testInterestSubjectType = dataFactory.makeInterestSubjectType('Ticker', null);
        insert testInterestSubjectType;
        
       	T1C_Base__Interest_Subject__c testInterestSubject = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK', null);
        insert testInterestSubject;

        // Test Data
        Map<String, String> interestWithContactNameTypo = new Map<String, String>{'Contact Name' => 'Dukk Mnn', 'Ticker Name' => 'DUCK'};
        Map<String, String> interestWithTickerNameTypo = new Map<String, String>{'Contact Name' => 'Duck Man', 'Ticker Name' => 'DUC'};

        List<Map<String, String>> testInterests = new List<Map<String, String>>{interestWithContactNameTypo, interestWithTickerNameTypo};      
        String contactNameField = 'Contact Name';
        String tickerNameField = 'Ticker Name';
        String companyNameField = 'Company Name';
        String contactEmailField = 'Contact Email';
        Map<String, BulkInterestLoaderController.Suggestion> prevSuggestions = new Map<String, BulkInterestLoaderController.Suggestion>
        {
            'Dukk Mnn' => new BulkInterestLoaderController.Suggestion('Contact Name', 'Duck Man', null, 0, 'RENAME', 2),
            'DUC'	   => new BulkInterestLoaderController.Suggestion('Ticker Name', 'DUCK', null, 0, 'RENAME', 1)
        };
        Id lastQueriedId = '000000000000000000';

		// Perform test
        Test.startTest();
        List<Object> matchInterestsResult = BulkInterestLoaderController.matchInterests(testInterests, testAccount.Id, contactNameField, tickerNameField, companyNameField, contactEmailField, prevSuggestions, lastQueriedId);        
        Test.stopTest();
        
        Map<String, BulkInterestLoaderController.Suggestion> expectedSuggestions = new Map<String, BulkInterestLoaderController.Suggestion>
        {
            'Dukk Mnn' => new BulkInterestLoaderController.Suggestion('Contact Name', 'Duck Man', null, 0, 'RENAME', 2),
            'DUC'	   => new BulkInterestLoaderController.Suggestion('Ticker Name', 'DUCK', null, 0, 'RENAME', 1)
        };
        
        System.assert(((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).containsKey('Dukk Mnn'));
        System.assert(((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).containsKey('DUC'));
        System.assertEquals(expectedSuggestions.get('Dukk Mnn').newValue, ((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).get('Dukk Mnn').newValue);
        System.assertEquals(expectedSuggestions.get('DUC').newValue, ((Map<String, BulkInterestLoaderController.Suggestion>)matchInterestsResult[0]).get('DUC').newValue);
    }
    
    @isTest static void TestGetMatchedRecords()
    {
		// Set up Test Database
        setAFRs();
        
        Account testAccount = dataFactory.makeAccount('Duck Land');
        insert testAccount;
        
        Contact testContact = dataFactory.makeContact(testAccount.Id, 'Duck', 'Man');
        insert testContact;
        
		T1C_Base__Interest_Subject_Type__c testInterestSubjectType = dataFactory.makeInterestSubjectType('Ticker', null);
        insert testInterestSubjectType;
        
       	T1C_Base__Interest_Subject__c testInterestSubject1 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK1', null);
        T1C_Base__Interest_Subject__c testInterestSubject2 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK2', null);
        insert new List<T1C_Base__Interest_Subject__c>
        {
            testInterestSubject1,
            testInterestSubject2
        };
            
        T1C_Base__Interest__c testInterest = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject1.Id);
        insert testInterest;
        
        // Test Data
        String contactName = 'Duck Man';
        String tickerName1 = 'DUCK1';
        String tickerName2 = 'DUCK2';
        Map<String, String> interest1 = new Map<String, String>{'Contact Name' => contactName, 'Ticker Name' => tickerName1};
        Map<String, String> interest2 = new Map<String, String>{'Contact Name' => contactName, 'Ticker Name' => tickerName2};

        List<Map<String, String>> testInterests = new List<Map<String, String>>{interest1, interest2};
        String contactNameField = 'Contact Name';
        String tickerNameField = 'Ticker Name';
        Map<String, String> errorToErrorAlias = new Map<String, String>();
        
        // Perform test
        Test.startTest();
        BulkInterestLoaderController.OutputInterestsAndErrors getMatchedRecordsResult = BulkInterestLoaderController.getMatchedRecords(testInterests, testAccount.Id, contactNameField, tickerNameField, errorToErrorAlias);        
        Test.stopTest();
        
        T1C_Base__Interest__c expectedInterest1 = new T1C_Base__Interest__c(T1C_Base__Account__c=testAccount.Id, 
                            T1C_Base__Contact__c=testContact.Id, 
                            T1C_Base__Interest_Subject__c=testInterestSubject1.Id,
                            T1C_Base__Interest_Subject_Type__c=testInterestSubjectType.Id,
                            T1C_Base__Reason__c='Interest',
                            T1C_Base__Source__c='Focus List',
                            T1C_Base__Expired__c=false);
        T1C_Base__Interest__c expectedInterest2 = new T1C_Base__Interest__c(T1C_Base__Account__c=testAccount.Id, 
                            T1C_Base__Contact__c=testContact.Id, 
                            T1C_Base__Interest_Subject__c=testInterestSubject2.Id,
                            T1C_Base__Interest_Subject_Type__c=testInterestSubjectType.Id,
                            T1C_Base__Reason__c='Interest',
                            T1C_Base__Source__c='Focus List',
                            T1C_Base__Expired__c=false);
        
        BulkInterestLoaderController.OutputInterest outputInterest1 = new BulkInterestLoaderController.OutputInterest(expectedInterest1, contactName, tickerName1, '');
        BulkInterestLoaderController.OutputInterest outputInterest2 = new BulkInterestLoaderController.OutputInterest(expectedInterest2, contactName, tickerName1, '');
        BulkInterestLoaderController.OutputInterestsAndErrors expectedOutput = new BulkInterestLoaderController.OutputInterestsAndErrors(new List<BulkInterestLoaderController.OutputInterest>{outputInterest1, outputInterest2}, new Map<Integer, String>());
        
        System.assert(expectedOutput.interests.size() == getMatchedRecordsResult.interests.size());
        System.assertEquals(expectedOutput.interests[0].interest.get('T1C_Base__Contact__c'), getMatchedRecordsResult.interests[0].interest.get('T1C_Base__Contact__c'));
        System.assertEquals(expectedOutput.interests[1].interest.get('T1C_Base__Contact__c'), getMatchedRecordsResult.interests[1].interest.get('T1C_Base__Contact__c'));
        System.assertEquals(expectedOutput.interests[0].interest.get('T1C_Base__Interest_Subject__c'), getMatchedRecordsResult.interests[0].interest.get('T1C_Base__Interest_Subject__c'));
        System.assertEquals(expectedOutput.interests[1].interest.get('T1C_Base__Interest_Subject__c'), getMatchedRecordsResult.interests[1].interest.get('T1C_Base__Interest_Subject__c'));
    }
    
    @isTest static void TestGetExpiringInterests()
    {
		// Set up Test Database
        setAFRs();
        
        Account testAccount = dataFactory.makeAccount('Duck Land');
        insert testAccount;
        
        Contact testContact = dataFactory.makeContact(testAccount.Id, 'Duck', 'Man');
        insert testContact;
        
		T1C_Base__Interest_Subject_Type__c testInterestSubjectType = dataFactory.makeInterestSubjectType('Ticker', null);
        insert testInterestSubjectType;
        
       	T1C_Base__Interest_Subject__c testInterestSubject1 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK1', null);
        T1C_Base__Interest_Subject__c testInterestSubject2 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK2', null);
        T1C_Base__Interest_Subject__c testInterestSubject3 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK3', null);
        insert new List<T1C_Base__Interest_Subject__c>
        {
            testInterestSubject1,
            testInterestSubject2
        };
            
        T1C_Base__Interest__c testInterest1 = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject1.Id);
        T1C_Base__Interest__c testInterest2 = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject2.Id); 
        insert new List<T1C_Base__Interest__c>
        {
            testInterest1,
            testInterest2
        };
            
        // Test Data
        Map<String, String> inputInterest = new Map<String, String>
        {
            'T1C_Base__Account__c' => testAccount.Id, 
            'T1C_Base__Contact__c' => testContact.Id,
            'T1C_Base__Interest_Subject_Type__c' => testInterestSubjectType.Id,
            'T1C_Base__Interest_Subject__c' => testInterestSubject3.Id
        }; 
        List<Id> contactIdsList = new List<Id>{testContact.Id};
        
        // Perform test
        Test.startTest();
        List<BulkInterestLoaderController.InterestRecord> getMatchedRecordsResult = BulkInterestLoaderController.getExpiringInterests(new List<Map<String, String>>{inputInterest}, contactIdsList, testAccount.Id, 'T1C_Base__Contact__c', 'T1C_Base__Interest_Subject__c');       
        Test.stopTest();
        
        System.assert(getMatchedRecordsResult.size() == 2);
        System.assertEquals('Duck Man', getMatchedRecordsResult[0].data.T1C_Base__Contact__r.Name);
        System.assertEquals('Duck Man', getMatchedRecordsResult[1].data.T1C_Base__Contact__r.Name);
        System.assertEquals('DUCK1', getMatchedRecordsResult[0].data.T1C_Base__Interest_Subject__r.Name);
        System.assertEquals('DUCK2', getMatchedRecordsResult[1].data.T1C_Base__Interest_Subject__r.Name);		
    }
    
    @isTest static void TestExpireInterests()
    {
		// Set up Test Database
        setAFRs();
        
        Account testAccount = dataFactory.makeAccount('Duck Land');
        insert testAccount;
        
        Contact testContact = dataFactory.makeContact(testAccount.Id, 'Duck', 'Man');
        insert testContact;
        
		T1C_Base__Interest_Subject_Type__c testInterestSubjectType = dataFactory.makeInterestSubjectType('Ticker', null);
        insert testInterestSubjectType;
        
       	T1C_Base__Interest_Subject__c testInterestSubject1 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK1', null);
        T1C_Base__Interest_Subject__c testInterestSubject2 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK2', null);
        insert new List<T1C_Base__Interest_Subject__c>
        {
            testInterestSubject1,
            testInterestSubject2
        };
        
        // Test Data        
       	T1C_Base__Interest__c testInterest1 = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject1.Id);
        T1C_Base__Interest__c testInterest2 = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject2.Id); 
        List<T1C_Base__Interest__c> interestsBefore = new List<T1C_Base__Interest__c>{testInterest1, testInterest2};
        insert interestsBefore;
        
        // Perform test
        Test.startTest();
        BulkInterestLoaderController.expireInterests(interestsBefore);
        Test.stopTest();
        
        List<T1C_Base__Interest__c> interestsAfter = [SELECT Id, T1C_Base__Expired__c FROM T1C_Base__Interest__c WHERE T1C_Base__Contact__c = :testContact.Id];
        
        System.assert(interestsAfter.size() == 2);
        System.assertEquals(true, interestsAfter[0].get('T1C_Base__Expired__c'));
        System.assertEquals(true, interestsAfter[1].get('T1C_Base__Expired__c'));
    }
    
    @isTest static void TestSaveInterests()
    {
		// Set up Test Database
        setAFRs();
        
        Account testAccount = dataFactory.makeAccount('Duck Land');
        insert testAccount;
        
        Contact testContact = dataFactory.makeContact(testAccount.Id, 'Duck', 'Man');
        insert testContact;
        
		T1C_Base__Interest_Subject_Type__c testInterestSubjectType = dataFactory.makeInterestSubjectType('Ticker', null);
        insert testInterestSubjectType;
        
       	T1C_Base__Interest_Subject__c testInterestSubject1 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK1', null);
        T1C_Base__Interest_Subject__c testInterestSubject2 = dataFactory.makeInterestSubject(testInterestSubjectType.id, 'DUCK2', null);
        insert new List<T1C_Base__Interest_Subject__c>
        {
            testInterestSubject1,
            testInterestSubject2
        };
        
        // Test Data
       	T1C_Base__Interest__c testInterest1 = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject1.Id);
        T1C_Base__Interest__c testInterest2 = dataFactory.makeInterest(testAccount.Id, testContact.Id, testInterestSubjectType.Id, testInterestSubject2.Id); 
        List<T1C_Base__Interest__c> interestsToSave = new List<T1C_Base__Interest__c>{testInterest1, testInterest2};
        
        // Perform test
        Test.startTest();
        BulkInterestLoaderController.saveInterests(interestsToSave);
        Test.stopTest();
        
        List<T1C_Base__Interest__c> interestsAfter = [SELECT Id, T1C_Base__Interest_Subject__c FROM T1C_Base__Interest__c WHERE T1C_Base__Contact__c = :testContact.Id];
        
        System.assert(interestsAfter.size() == 2);
        System.assertEquals(testInterestSubject1.Id, interestsAfter[0].T1C_Base__Interest_Subject__c);
        System.assertEquals(testInterestSubject2.Id, interestsAfter[1].T1C_Base__Interest_Subject__c);        
    }
}