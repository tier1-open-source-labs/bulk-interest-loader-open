namespace ACE
{
    class LoadWindowController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;
    
        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = context.options;
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            this._context.local.eventDispatcher.on("ready", function(e : ACE.ACEEvent, eventData : any)
            { 
                this._context.local.state.put("windowIsReady", true);
            }.bind(this));

            ACE.CustomEventDispatcher.on(ACE.Event.CHILD_APPLICATION_BRIDGE_LOADED, function()
            {
                return false;
            });

            callback(true);
        }
    }

    ACE.AML.ControllerManager.register("extensions:LoadWindowController", LoadWindowController, true);
}