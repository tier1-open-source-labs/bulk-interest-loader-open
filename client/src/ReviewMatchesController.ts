interface Suggestion {
    id                  : string,
    oldValue            : string,
    newValue            : string,
    displayValue        : string,
    interestIndices     : number[],
    field               : string,
    error               : string,
    altSuggestionId  : string,
    _customIsSelected   : boolean
}

namespace ACE
{
    var BASE_CONFIG = 
    {
        maxRemotingCallIterations   : 10,
        recommendedMatchesProperty  : "recommendedMatches.gridData",
        contactsNotFoundProperty    : "contactsNotFound.gridData",
        accountIdParameter          : "entityId",
        batchSize                   : 100,
        gridStep                    : 0
    }

    class ReviewMatchesController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;

        private CONTACT_NAME_COLUMN = ACE.Locale.getBundleValue("CONTACT_NAME_COLUMN");
        private TICKER_NAME_COLUMN = ACE.Locale.getBundleValue("TICKER_NAME_COLUMN");
        private COMPANY_NAME_COLUMN = ACE.Locale.getBundleValue("COMPANY_NAME_COLUMN");
        private CONTACT_EMAIL_COLUMN = ACE.Locale.getBundleValue("CONTACT_EMAIL_COLUMN");

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var interests : object[] = this._context.local.state.get("interests");
            var accountId : string = ACEUtil.getURLParameter(this._options.accountIdParameter);
            this._context.local.state.put("latestChangesStep", this._options.gridStep);

            // context.services.execute(func name, args).then()
            var numCalls = 0;
            var reviewMatches : Function = function(suggestions, lastQueriedId, numCalls){
                ACE.Remoting.batch("BulkInterestLoaderController.matchInterests", interests, function(args)
                {
                    return [args, accountId, this.CONTACT_NAME_COLUMN, this.TICKER_NAME_COLUMN, 
                        this.COMPANY_NAME_COLUMN, this.CONTACT_EMAIL_COLUMN, suggestions, lastQueriedId];
                }.bind(this), function(result, eventData)
                {
                    var suggestionsData : Suggestion[] = result.map(function(batch)
                    {
                        return batch[0];
                    }).reduce(function(accumulator, currentValue)
                    {
                        return {...accumulator, ...currentValue};
                    }, {});

                    if (eventData)
                    {
                        if (numCalls > this._options.maxRemotingCallIterations)
                        {
                            callback(false);
                        }
                        else if (result[0][2] < 10000)
                        {
                            // All the interest subject records were queried, so we have the best matches
                            var recommendedMatchesData : Suggestion[] = [];
                            var contactsNotFoundData : Suggestion[] = [];
                            var oldValues : string[] = Object.keys(suggestionsData);
                            
                            for (let i = 0; i < oldValues.length; i++)
                            {
                                var error = suggestionsData[oldValues[i]].type.toLowerCase() + suggestionsData[oldValues[i]].field.replace(/\s/g, "");

                                switch(error)
                                {
                                    case "renameTickerName":
                                        error = "Interest Subjects";
                                        break;
                                    case "renameContactName":
                                        error = "Contacts";
                                        break;
                                    case "createContactName":
                                        error = "Contacts Not Found";
                                        break;
                                }

                                var displayValue : string = suggestionsData[oldValues[i]].newValue + 
                                    (suggestionsData[oldValues[i]].extraValue ? ` (${suggestionsData[oldValues[i]].extraValue})` : '');

                                if (error === "Contacts Not Found")
                                {
                                    contactsNotFoundData.push({
                                        id: i + "createContactName",
                                        oldValue: oldValues[i], 
                                        newValue: oldValues[i],
                                        displayValue: displayValue,
                                        interestIndices: [],
                                        field: suggestionsData[oldValues[i]].field,
                                        error: error,
                                        altSuggestionId: "-1",
                                        _customIsSelected: false
                                    });
                                }
                                else if (error == "Contacts")
                                {
                                    recommendedMatchesData.push({
                                        id: i + "renameContactName",
                                        oldValue: oldValues[i], 
                                        newValue: suggestionsData[oldValues[i]].newValue,
                                        displayValue: displayValue,
                                        interestIndices: suggestionsData[oldValues[i]].interestIndices, 
                                        field: suggestionsData[oldValues[i]].field,
                                        error: error,
                                        altSuggestionId: i + "createContactName",
                                        _customIsSelected: false
                                    });
                                    contactsNotFoundData.push({
                                        id: i + "createContactName",
                                        oldValue: oldValues[i], 
                                        newValue: oldValues[i],
                                        displayValue: displayValue,
                                        interestIndices: [],
                                        field: suggestionsData[oldValues[i]].field,
                                        error: "Contacts Not Found",
                                        altSuggestionId: i + "renameContactName",
                                        _customIsSelected: false
                                    });
                                }
                                else
                                {
                                    recommendedMatchesData.push({
                                        id: i + "renameTickerName",
                                        oldValue: oldValues[i], 
                                        newValue: suggestionsData[oldValues[i]].newValue,
                                        displayValue: displayValue,
                                        interestIndices: suggestionsData[oldValues[i]].interestIndices,
                                        field: suggestionsData[oldValues[i]].field,
                                        error: error,
                                        altSuggestionId: "-1",
                                        _customIsSelected: false
                                    });                 
                                }
                            }

                            this._context.local.state.put(this._options.recommendedMatchesProperty, recommendedMatchesData);
                            this._context.local.state.put(this._options.contactsNotFoundProperty, contactsNotFoundData);

                            callback(true);
                        }
                        else
                        {
                            // There are still interest subject ideas we have not fuzzy matched yet
                            reviewMatches(suggestionsData, result[0][1], numCalls + 1)
                        }
                    }
                    else
                    {
                        console.log("matchInterests batch remote call failed");
                        callback(false);
                    }
                }.bind(this),
                {
                    retryOnTimeout : false,
                    batchSize : this._options.batchSize,
                    showProgressBar : true,
                    alwaysShowProgressBar : true,
                    parallel: true
                },
                {
                    retryOnTimeout : false,
                    batchSize : this._options.batchSize,
                    showProgressBar : true,
                    alwaysShowProgressBar : true,
                    parallel: true
                });
            }.bind(this);
            reviewMatches({}, "000000000000000000", numCalls);
        }
    }

    ACE.AML.ControllerManager.register("extensions:ReviewMatchesController", ReviewMatchesController, true);
}