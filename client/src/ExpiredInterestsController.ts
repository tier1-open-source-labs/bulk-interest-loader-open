namespace ACE
{
    class ExpiredInterestsController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;
        private BASE_OPTIONS : any = 
        {
            property                : "expiringInterests",
            contIdField             : "T1C_Base__Contact__c",
            intSubIdField           : "T1C_Base__Interest_Subject__c",
            fieldPrefix             : "interest",
            dataProviderProperty    : "foundRecords.gridData",
            accountIdParameter      : "entityId",
            latestStepWithChanges   : "latestChangesStep",
            gridStep                : 0
        };

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, this.BASE_OPTIONS, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var interestsData : {_customIsSelected : boolean}[] = this._context.local.state.get(this._options.dataProviderProperty);
            var interests : object[] = []
            var contactIds : string[] = [];

            // update state with this step as containing the most recent changes
            this._context.local.state.put(this._options.latestStepWithChanges, this._options.gridStep);

            if(this._options.fieldPrefix && this._options.fieldPrefix.length > 0)
            {
                for (let i = 0; i < interestsData.length; i++)
                {
                    var interest = interestsData[i];
                    contactIds.push(interest[this._options.fieldPrefix]["T1C_Base__Contact__c"]);

                    if (interest._customIsSelected)
                    {
                        interests.push(interest[this._options.fieldPrefix]);
                    }
                }
            }

            // Make url param an option
            var accountId : string = ACEUtil.getURLParameter(this._options.accountIdParameter);

            // context.services.execute(func name, args).then()
            ACE.Remoting.call("BulkInterestLoaderController.getExpiringInterests", [interests, contactIds, accountId, this._options.contIdField, this._options.intSubIdField], 
            function(result, eventData)
            {
                if (eventData.status)
                {
                    this._context.local.state.put(this._options.property, result);

                    callback(true);
                }
                else
                {
                    console.log("getExpiringInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        }
    }

    ACE.AML.ControllerManager.register("extensions:ExpiredInterestsController", ExpiredInterestsController, true);
}