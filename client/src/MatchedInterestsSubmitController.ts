namespace ACE
{
    var BASE_CONFIG = 
    {
        dataProviderProperty : "filteredMatchedRecords"
    };

    class MatchedInterestsSubmitController
    {
        protected _context : ACE.AML.ControllerContext;
        protected _options : any;

        /**
        * Constructor
        *
        * @param context Context passed in from ACE.AML.DOMUtil.load
        */
        constructor(context : ACE.AML.ControllerContext)
        {
            this._context = context;
            this._options = $.extend({}, BASE_CONFIG, context.options);
        }

        /**
        * Executes the business logic for the controller
        *
        * @param e Event handler that fired the controller
        * @param eventData Event data tied to event that fired the controller
        * @param callBack The callback to tell the overall DOM Util when its complete
        */
        public execute(e : ACE.ACEEvent, eventData : any, callback : Function) : void
        {
            var matchedInterests = this._context.local.state.get(
                this._options.dataProviderProperty).map(function(record)
            {
                return record.interest;
            });

            // context.services.execute(func name, args).then()
            ACE.Remoting.call("BulkInterestLoaderController.saveInterests", [matchedInterests], 
            function(result, eventData)
            {
                if (eventData.status)
                {
                    callback(true);
                }
                else
                {
                    console.log("saveInterests remote call failed with status code " + eventData.statusCode);
                    callback(false);
                }
            }.bind(this), null, null);
        }
    }

    ACE.AML.ControllerManager.register("extensions:MatchedInterestsSubmitController", MatchedInterestsSubmitController, true);
}